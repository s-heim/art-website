<?php
	require_once "../../processing/data/repositories/artists_repository.php";
	require_once "../../processing/data/repositories/artworks_repository.php";
	
	class SearchFunctions {
	
		/*
		* Validates the given search parameter.
		*
		* A valid parameter must be set and not empty.
		* The method tests whether the input has a valid value or returns the default.
		* 
		* @param 	string		$input				The parameter value input to validate 
		* @param 	string		$default			The default value of this parameter
		* @param 	String[]	$range				An array of all valid values of this parameter
		* @return 	string
		*/
		static function validateParamInput($input, $default, $range) {
			if (isset($input) && (!empty($input))) {
				$input = trim($input);
				if (in_array($input, $range)) {
					return $input;
				}
			}
			return $default;
		}
		
		/*
		* Validates the given search key word.
		*
		* A valid parameter must be set and not empty.
		* 
		* @param 	string		$entity				The table to search through
		* @param 	string		$search				The search request
		* @param 	string		$sortdir			The direction to sort the result
		* @param 	string		$sortparam			The parameter to sort the result
		* @return 	string
		*/
		static function validateSearchInput($entity, $search, $sortdir, $sortparam) {
			if (isset($search) && (!empty($search))) {
				$search = trim($search);
				if ($entity == "artists") {
					$artistrepo = new ArtistsRepository();
					return $artistrepo->simpleSearchArtists($search, $sortdir, $sortparam);
				}
				else {
					$artworkrepo = new ArtworksRepository();
					$artistrepo = new ArtistsRepository();
					$artworks = $artworkrepo->simpleSearchArtworks($search, $sortdir, $sortparam);
					foreach($artworks as $artwork) {
						$artwork->setArtist($artistrepo->getArtistByID($artwork->getArtistID()));

					}
					return $artworks;
				}

			}
			return null;
		}
		
		/*
		* Validates the given search input for the advanced search
		*
		* A valid parameter must be set and not empty.
		* 
		* @param 	string		$entity				The table to search through
		* @param 	string		$keyword			The keyword for search (last name or title)
		* @param 	string		$keyword2			The second keyword for search (first name / only for entity artist)
		* @param 	int			$min				The minimum of the range to select (year of living or work)
		* @param 	int			$max				The minimum of the range to select (year of living or work)
		* @param 	string		$select				The selected attribute of the list (nationality or genre)
		* @param 	string		$select2			The second selected attribute of the list (subject / only for entity artworks)
		* @param 	string		$sortdir			The direction to sort the result
		* @param 	string		$sortparam			The parameter to sort the result
		* @return 	string
		*/
		static function validateAdvancedSearchInput($entity, $keyword, $keyword2, $min, $max, $select, $select2, $sortdir, $sortparam) {
			if ($entity == "artists") {
				$artistrepo = new ArtistsRepository();
				$artists = $artistrepo->advancedSearchArtists($keyword, $keyword2, $min, $max, $select, $sortdir, $sortparam);
				return $artists;
			}
			else if ($entity == "artworks")  {
				$artworkrepo = new ArtworksRepository();
				$artistrepo = new ArtistsRepository();
				$artworks = $artworkrepo->advancedSearchArtworks($keyword, $min, $max, $select, $select2, $sortdir, $sortparam);
				foreach($artworks as $artwork) {
					$artwork->setArtist($artistrepo->getArtistByID($artwork->getArtistID()));

				}
				return $artworks;
			}
			return null;
		}
		
		/*
		* Gives an array with all relevant datails about the saved favorites
		* 
		* @param 	string		$entity				The table to search through
		* @param 	String[]	$ids				The ids of the saved favorites
		* @param 	string		$sortdir			The direction to sort the result
		* @param 	string		$sortparam			The parameter to sort the result
		* @return 	String[]
		*/
		static function getFavorites($entity, $ids, $sortdir, $sortparam) {
			if ($entity == "artists") {
				$artistrepo = new ArtistsRepository();
				return $artistrepo->getArtistsByIDs($ids, $sortdir, $sortparam);
			}
			else {
				$artworkrepo = new ArtworksRepository();
				$artistrepo = new ArtistsRepository();
				$artworks = $artworkrepo->getArtWorksByIDs($ids, $sortdir, $sortparam);
				if ($artworks != null) {
					foreach($artworks as $artwork) {
						$artwork->setArtist($artistrepo->getArtistByID($artwork->getArtistID()));
	
					}
				}
				
				return $artworks;
			}
		}
		
		/*
		* Gives a form to sort artists by the last name / artworks by the title, msrp or year
		* 
		* @param 	string		$entity				The table to search through
		* @param 	string		$sortparams			The possible sorting parameters for this entity
		* @param 	string		$setSortDir			The actual sorting direction for this entity
		* @return 	string
		*/
		static function getSortBox($entity, $sortparams, $setSortDir) {
			$form = "<div class='form-check form-check-inline'>";
			$form .= "<input type='radio' class='form-check-input' id='ASCradio' name='sort" . $entity . "dir' value='asc' ";
			if ($setSortDir == "asc" || $setSortDir == null) {
				$form .= "checked";
			}
			$form .= ">";
			$form .= "<label class='form-check-label' for='ASCradio'>asc</label>";
			$form .= "</div>";
			$form .= "<div class='form-check form-check-inline'>";
			$form .= "<input type='radio'  class='form-check-input' id='DESCradio' name='sort" . $entity . "dir' value='desc' ";
			if ($setSortDir == "desc") {
				$form .= "checked";
			}
			$form .= ">";
			$form .= "<label class='form-check-label' for='DESCradio'>desc</label>";
			$form .= "</div>";
			foreach($sortparams as $sortparam) {
				$form .= "<div class='d-inline mr-3'>";
				$form .= "<button type='submit' class='btn btn-primary' name='sort" . $entity . "' value='" . $sortparam . "'>";
				$form .= "Sort by " . $sortparam;
				$form .= "</button>";
				$form .= "</div>";
			}
			return  $form;
		}

		/*
		* Gives a button to delete an artist or artwork from the favorite list
		*
		* @param 	string		$type				The type of the actual shown content
		* @param 	int			$id					The id of the artwork / artist to delete
		* @param 	string		$sortartworksdir	The actual sorting direction of the artworks
		* @param 	string		$sortartistsdir		The actual sorting direction of the artists
		* @param 	string		$sortartists		The actual sorting parameter of the artists
		* @param 	string		$sortartworks		The actual sorting parameter of the artworks
		* @return 	string
		*/
		static function getFavoriteRemoveButton($type, $id, $sortartworksdir, $sortartistsdir, $sortartists, $sortartworks) {
			$button = "<form action='../utilities/favorite_list.php' method='get'>";
			$button .= "<input type='hidden' name='sortartworksdir' value='" . $sortartworksdir . "' />";
			$button .= "<input type='hidden' name='sortartistsdir' value='" . $sortartistsdir . "' />";
			$button .= "<input type='hidden' name='sortartists' value='" . $sortartists . "' />";
			$button .= "<input type='hidden' name='sortartworks' value='" . $sortartworks . "' />";
			$button .= "<button type='submit' class='btn btn-secondary btn-block btn-sm mt-1' name='" . $type . "' value='" . $id . "' class='btn btn-primary btn-sm'>";
			$button .= "Delete from list";
			$button .= "</button>";
			$button .= "</form>";
			return $button;
		}
		
		/*
		* Shows all matching artists of a search / saved favorite artists in boxes
		* 
		* If it shall be possible to delete an artist from this box, there are the actual sorting parameters required
		*
		* @param 	String[]	$artists			An array with all relevant details about the artists
		* @param 	string		$size				The needed size of the boxes (lg or sm)
		* @param 	string 		$type				Is "delfavartist" if it shall be possible to delete an artist from this box 
		* @param 	string		$sortartworksdir	The actual sorting direction of the artworks (needed if it shall be possible to delete an artist from this box)
		* @param 	string		$sortartistsdir		The actual sorting direction of the artists (needed if it shall be possible to delete an artist from this box)
		* @param 	string		$sortartists		The actual sorting parameter of the artists (needed if it shall be possible to delete an artist from this box)
		* @param 	string		$sortartworks		The actual sorting parameter of the artworks (needed if it shall be possible to delete an artist from this box)
		* @return 	string
		*/
		static function showMatchingArtists($artists, $size=null, $type=null, $sortartworksdir=null, $sortartistsdir=null, $sortartists=null, $sortartworks=null)  {
			$box = "";
			if ($artists == null) {
				return $box;
			}
			else {
				foreach($artists as $artist) {
					if ($size == "lg") {
						$box .= "<div class='col-2 mb-3'>";
					}
					else {
						$box .= "<div class='col-4 mb-3'>";
					}
					$box .= $artist->getShortDetails();
					if ($type == "delfavartist") {
						$box .= self::getFavoriteRemoveButton($type, $artist->getID(), $sortartworksdir, $sortartistsdir, $sortartists, $sortartworks);
					}
					$box .= "</div>";
				}
				return $box;
			}
		}
		
		/*
		* Shows all matching artworks of a search / saved favorite artworks in boxes
		* 
		* If it shall be possible to delete an artist from this box, there are the actual sorting parameters required
		*
		* @param 	String[]	$artworks			An array with all relevant details about the artworks
		* @param 	string		$size				The needed size of the boxes (lg or sm)
		* @param 	string 		$type				Is "delfavartist" if it shall be possible to delete an artist from this box 
		* @param 	string		$sortartworksdir	The actual sorting direction of the artworks (needed if it shall be possible to delete an artist from this box)
		* @param 	string		$sortartistsdir		The actual sorting direction of the artists (needed if it shall be possible to delete an artist from this box)
		* @param 	string		$sortartists		The actual sorting parameter of the artists (needed if it shall be possible to delete an artist from this box)
		* @param 	string		$sortartworks		The actual sorting parameter of the artworks (needed if it shall be possible to delete an artist from this box)
		* @return 	string
		*/
		static function showMatchingArtworks($artworks, $size=null, $type=null, $sortartworksdir=null, $sortartistsdir=null, $sortartists=null, $sortartworks=null) {
			$box = "";
			if ($artworks == null) {
				return $box;
			}
			else {
				foreach($artworks as $artwork) {
					if ($size == "lg") {
						$box .= "<div class='col-2 mb-3'>";
					}
					else {
						$box .= "<div class='col-4 mb-3'>";
					}
					$box .= $artwork->getShortDetails("both");
					if ($type == "delfavartwork") {
						$box .= self::getFavoriteRemoveButton($type, $artwork->getID(), $sortartworksdir, $sortartistsdir, $sortartists, $sortartworks);
					}
					$box .= "</div>";
				}
				return $box;
			}
		}
	
	}
	
?>