<?php 
	require_once "../../processing/data/repositories/genres_repository.php";
	require_once "../../processing/data/repositories/subjects_repository.php";
	require_once "../../processing/data/repositories/customers_repository.php";
	
	class PrepareFunctions {
	
		/*
		* Validates a given id of its syntax
		*
		* A valid id must be set and a numeric. 
		* Invalid ids return the default value 0.
		* 
		* @param 	string		$id			The id to validate
		* @return 	string
		*/
		static function validateIdInput($id) {
			if (isset($id) && is_numeric($id)) {
				return $id;
			}
			return 0;
		}
		
		/*
		* Puts all relevant details of a given user in an array
		* 
		* @param 	int			$id			The id of the requested user
		* @return 	String[]
		*/
		static function getSingleUserDetails($id) {
			$customerrepo = new CustomersRepository();
			$customer = $customerrepo->getCustomerByID($id);
			return $customer;
		}
		
		/*
		* Puts all genre names in an array
		* 
		* @return 	String[]
		*/
		static function getAllGenresForSearch() {
			$genrerepo = new GenresRepository();
			$genres = $genrerepo->getAllGenres("name");
			$list = array();
			foreach($genres as $genre) {
				$list[] = $genre->getName();
			}
			return $list;
		}
		
		/*
		* Puts all subjects names in an array
		* 
		* @return 	String[]
		*/
		static function getAllSubjectsForSearch() {
			$subjectrepo = new SubjectsRepository();
			$subjects = $subjectrepo->getAllSubjects();
			$list = array();
			foreach($subjects as $subject) {
				$list[] = $subject->getName();
			}
			return $list;
		}
		
		/*
		* Puts all nationalities in an array
		* 
		* @return 	String[]
		*/
		static function getAllNationalitiesForSearch() {
			$artistrepo = new ArtistsRepository();
			$nations = $artistrepo->getAllNationalities();
			$list = array();
			foreach($nations as $nation) {
				$list[] = $nation["Nationality"];
			}
			return $list;
		}
		
	}
 
?>