<?php 
	class ShowFunctions {
		
		/* 
		* Gives a button to set an artist or artwork on the favorite list
		* 
		* @param 	string		$type				The type of the actual shown content
		* @param 	int			$id					The id of the actual shown content
		* @param 	bool		$isset				True, if the actual content is already on the favorites list
		* @return 	string
		*/
		static function getFavoriteButton($type, $id, $isset) {
			$button = "<form action='../utilities/favorite_list.php' method='post'>";
			if ($isset) {
				$button .= "<button class='btn btn-secondary'>";
				$button .= "Already on your Favorites List";
				$button .= "</button>";
			}
			else {
				$button .= "<button type='submit' name='" . $type . "' value='" . $id . "' class='btn btn-primary'>";
				$button .= "Add to Favorites List";
				$button .= "</button>";
			}
			$button .= "</form>";
			return $button;
		}
		
		
		
	}
	
?>