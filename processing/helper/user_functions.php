<?php
	require_once "../../processing/data/repositories/customers_repository.php";
	require_once "../../processing/data/repositories/reviews_repository.php";
	
	class UserFunctions {
	
		/*
		* Checks whether the logged in customer already reviewed the actual shown artwork
		* 
		* @param 	int 	$artworkID	The id of the actual shown artwork
		* @param 	int		$customerID	The id of the actual logged in customer
		* @return 	boolean
		*/
		static function checkForReview($artworkID, $customerID) {
			$reviewrepo = new ReviewsRepository();
			$review = $reviewrepo->getReviewForArtworkByCustomer($artworkID, $customerID);
			if ($review->getID() != null) {
				return true;
			}
			return false;
		}
		
		/*
		* Checks whether the login input is valid or not 
		*
		* A valid login must contain a already registred user name and the correct password
		* 
		* @param 	string	$user		The user name to verify
		* @param 	string	$pass		The (already hashed) password to verify
		* @return 	boolean
		*/
		static function verifyUser($user, $pass) {
			$customerrepo = new CustomersRepository();
			$customer = $customerrepo->getCustomersByName($user);
			if ($customer->getID() == null) {
				return false;
			}
			return $customer->verify($pass);
		}
		
		/*
		* Checks whether the register input is valid or not 
		*
		* A valid register input is not allowed to be empty. Some fields have more requirements.
		* 
		* @param 	string	$input		The input to verify
		* @param 	string	$type		The kind of input
		* @return 	boolean
		*/
		static function validateRegisterInput($input, $type) {
			if (empty($input)) {
				return false;
			}
			switch($type) {
				case "username": 
					return self::isAvailableUserName($input);
				case "password":
					return self::isSecurePassword($input);
				case "email":
					return self::isAvailableEmail($input);
				default:
					return true;
			}
		}
		
		/*
		* Checks whether a user name is not already used
		*
		* @param 	string	$name		The name to look for
		* @return 	boolean
		*/
		static function isAvailableUserName($name) {
			$customerrepo = new CustomersRepository();
			$customer = $customerrepo->getCustomersByName($name);
			if ($customer->getID() == null) {
				return true;
			}
			return false;
		}
		
		/*
		* Checks whether a password is secure or not
		*
		* A secure password must be at least eight characters long, contain upper und lower letters and numbers.
		*
		* @param 	string	$pass		The password to check
		* @return 	boolean
		*/
		static function isSecurePassword($pass) {
			if (strlen($pass) < 8) {
				return false;
			}
			$numbercontrol = false;
			for ($i = 48; $i <= 57; $i++) {
				if (strstr($pass, chr($i))) {
					$numbercontrol = true;
					break;
				}
			}
			if (!($numbercontrol)) {
				return false;
			}
			$upperlettercontrol = false;
			for ($i = 65; $i <= 90; $i++) {
				if (strstr($pass, chr($i))) {
					$upperlettercontrol = true;
					break;
				}
			}
			if (!($upperlettercontrol)) {
				return false;
			}
			$lowerlettercontrol = false;
			for ($i = 97; $i <= 122; $i++) {
				if (strstr($pass, chr($i))) {
					$lowerlettercontrol = true;
					break;
				}
			}
			if (!($lowerlettercontrol)) {
				return false;
			}
			return true;
		}
		
		/*
		* Checks whether a email address is not already used
		*
		* @param 	string	$email		The email address to look for
		* @return 	boolean
		*/
		static function isAvailableEmail($email) {
			if (!(strstr($email, '@'))) {
				return false;
			}
			$customerrepo = new CustomersRepository();
			$customer = $customerrepo->getCustomersByEmail($email);
			if ($customer->getID() == null) {
				return true;
			}
			return false;
		}
		
		/*
		* Adds a new user to the database
		*
		* @param 	string	$username 	The user name to log in
		* @param 	string	$pass 		The password to log in
		* @param 	string	$firstname 	The first name of the new customer
		* @param 	string	$lastname 	The last name of the new customer
		* @param 	string	$address 	The address of the new customer
		* @param 	string	$city 		The city where the new customer lives
		* @param 	string	$region		The region where the new customer lives
		* @param 	string	$country 	The country where the new customer lives
		* @param 	string	$phone 		The phone number of the new customer
		* @param 	string	$email 		The email address of the new customer
		* @param 	int 	$privacy 	The privacy status of the new customer
		* @return 	boolean
		*/
		static function AddUser($username, $pass, $firstname, $lastname, $address, $city, $region, $country, $postal, $phone, $email, $privacy) {
			$pass = password_hash($pass, PASSWORD_DEFAULT);
			$customer = new Customer(0, $username, $pass, 1, null, null, strip_tags($firstname), strip_tags($lastname), strip_tags($address), strip_tags($city), strip_tags($region), strip_tags($country), strip_tags($postal), strip_tags($phone), strip_tags($email), $privacy, false);
			$customerrepo = new CustomersRepository();
			$customerrepo->addCustomer($customer);
			return true;
		}
		
		/*
		* Edits an active user
		*
		* @param 	int		$id 		The id of the customer to edit
		* @param 	string	$firstname 	The first name of the customer to edit
		* @param 	string	$lastname 	The last name of the customer to edit
		* @param 	string	$address 	The address of the customer to edit
		* @param 	string	$city 		The city where the customer to edit lives
		* @param 	string	$region		The region where the customer to edit lives
		* @param 	string	$country 	The country where the customer to edit lives
		* @param 	string	$postal 	The postal of the customer to edit
		* @param 	string	$phone 		The phone number of the customer to edit
		* @param 	string	$email 		The email address of the customer to edit
		* @param 	int 	$privacy 	The privacy status of the customer to edit
		* @return 	boolean
		*/
		static function EditUser($id, $firstname, $lastname, $address, $city, $region, $country, $postal, $phone, $email, $privacy) {
			$customerrepo = new CustomersRepository();
			$customer = $customerrepo->getCustomerByID($id);
			$customer->update(strip_tags($firstname), strip_tags($lastname), strip_tags($address), strip_tags($city), strip_tags($region), strip_tags($country), strip_tags($postal), strip_tags($phone), strip_tags($email), $privacy);
			$customerrepo->updateCustomer($customer);
			return true;
		}
		
		/*
		* Edits the password of an active user
		*
		* @param 	int	$id 			The id of the customer
		* @param 	string	$username 	The username of the customer
		* @param 	string	$oldpass 	The old password of the customer
		* @param 	string	$newpass 	The new password  of the customer
		* @return 	boolean
		*/
		static function EditUserPass($id, $username, $oldpass, $newpass) {
			if (self::verifyUser($username, $oldpass)) {
				$customerrepo = new CustomersRepository();
				$customer = $customerrepo->getCustomerByID($id);
				$customer->changePassword($newpass);
				$customerrepo->updateCustomerPass($customer);
				return true;
			}
			return false;
		}
		
		/*
		* Makes an user who is not already admin to an admin
		*
		* @param 	int		$userID 	The id of the user
		* @return 	boolean
		*/
		static function MakeUserToAdmin($id) {
			$customerrepo = new CustomersRepository();
			$customer = $customerrepo->getCustomerByID($id);
			$customerrepo->updateCustomerAdminStatus($customer);
			return true;
		}
		
		/*
		* Deletes all unnecessary details of an user and makes the account inactive
		*
		* @param 	int		$userID 	The id of the user to delete
		* @return 	boolean
		*/
		static function DeleteUser($userID) {
			$customerrepo = new CustomersRepository();
			$customer = $customerrepo->getCustomerByID($userID);
			$customerrepo->deactivateCustomer($customer);
			return true;
		}
		
		/*
		* Adds a new review to a artwork
		*
		* @param 	int		$artwork 	The id of the reviewed artwork
		* @param 	int		$customer 	The id of the reviewing customer
		* @param 	int		$rating 	The rating between 1 and 5 of the review
		* @param 	string	$comment 	The comment to the artwork
		*/
		static function addReviewForArtwork($artwork, $customer, $rating, $comment) {
			$reviewrepo = new ReviewsRepository();
			$review = $reviewrepo->addReview($artwork, $customer, $rating, strip_tags($comment, "<br>"));
		}
		
		/*
		* Deletes a artwork from the database
		*
		* @param 	int		$reviewID 	The id of the review to delete
		*/
		static function deleteReviewFromArtwork($reviewID) {
			$reviewrepo = new ReviewsRepository();
			$review = $reviewrepo->deleteReview($reviewID);
		}
		
	}
	
?>