<?php	
	require_once "../../processing/data/configuration/database.php";
	require_once "../../processing/data/models/customer.php";
	
	class CustomersRepository {
		
		/**
		* @var database $database Saves the connection to the database
		*/
		private $database;

		private function parse($data) {
			return Customer::getFromArray($data); 
		}

		private function parseMulti($data) {
			return DataHandler::getFromMultiArray(function($data) {
				return $this->parse($data);
			}, $data);
		}
		
		/*
		* Initializes the database object
		*/
		function __construct() {
			$this->database = new Database();
		}
		
		/*
		* Selects all customers from the table `customers`
		*
		* @return 	Customer[]
		*/
		public function getAllCustomers() {
			$this->database->connect();
			$sql = "SELECT CustomerID, FirstName, LastName, Country, IsAdmin ";
			$sql .= "FROM customers ";
			$sql .= "WHERE UserName IS NOT NULL";
			$data = $this->database->runStatement($sql);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
		/*
		* Selects the customer with the given id from the table `customers`
		*
		* @param 	int 	$id 		The id of the requestet customer
		* @return 	Customer
		*/
		public function getCustomerByID($id) {
			$this->database->connect();
			$sql = "SELECT CustomerID, UserName, Pass, FirstName, LastName, Address, Postal, City, Region, Country, Phone, Email, Privacy, IsAdmin ";
			$sql .= "FROM customers ";
			$sql .= "WHERE customers.CustomerID = :id";
			$data = $this->database->runStatement($sql, [ [":id", $id] ]);
			$this->database->close();
			if (count($data) < 1) {
				return $this->parse($data);
			}
			return $this->parse($data[0]);
		}
		
		/*
		* Selects the customer with the given user name from the table `customers`
		*
		* @param 	string	$username	The user name of the requestet customer
		* @return 	Customer
		*/
		public function getCustomersByName($username) {
			$this->database->connect();
			$sql = "SELECT CustomerID, UserName, Pass, IsAdmin ";
			$sql .= "FROM customers ";
			$sql .= "WHERE customers.UserName = :username";
			$data = $this->database->runStatement($sql, [ [":username", $username] ]);
			$this->database->close();
			if (count($data) < 1) {
				return $this->parse($data);
			}
			return $this->parse($data[0]);
		}
		
		/*
		* Selects the customer with the given email address name from the table `customers`
		*
		* @param 	string	$email 		The email address of the requestet customer
		* @return 	Customer
		*/
		public function getCustomersByEmail($email) {
			$this->database->connect();
			$sql = "SELECT CustomerID ";
			$sql .= "FROM customers ";
			$sql .= "WHERE customers.Email = :email";
			$data = $this->database->runStatement($sql, [ [":email", $email] ]);
			$this->database->close();
			if (count($data) < 1) {
				return $this->parse($data);
			}
			return $this->parse($data[0]);
		}
		
		/*
		* Adds a new customer to the table `customers`
		*
		* @param 	Customer	$customer 	The customer
		*/
		public function addCustomer($customer) {
			$this->database->connect();
			$sql = "INSERT INTO customers ";
			$sql .= "(UserName, Pass, State, DateJoined, DateLastModified, FirstName, LastName, Address, City, Region, Country, Postal, Phone, Email, Privacy)";
			$sql .= "VALUES (:username, :pass, 1, NOW(), NOW(), :firstname, :lastname, :address, :city, :region, :country, :postal, :phone, :email, :privacy)";
			$this->database->runStatement($sql, [], $customer, "add");
			$this->database->close();
		}
		
		/*
		* Updates a already saved customer in the table `customers`
		*
		* @param 	Customer	$customer 	The customer
		*/
		public function updateCustomer($customer) {
			$this->database->connect();
			$sql = "UPDATE customers SET ";
			$sql .= 	"DateLastModified = NOW(), ";
			$sql .= 	"FirstName = :firstname, ";
			$sql .= 	"LastName = :lastname, ";
			$sql .= 	"Address = :address, ";
			$sql .= 	"City = :city, ";
			$sql .= 	"Region = :region, ";
			$sql .= 	"Country = :country, ";
			$sql .= 	"Postal = :postal, ";
			$sql .= 	"Phone = :phone, ";
			$sql .= 	"Email = :email, ";
			$sql .= 	"Privacy = :privacy ";
			$sql .= "WHERE CustomerID = :id";
			$this->database->runStatement($sql, [], $customer, "update");
			$this->database->close();
		}
		
		/*
		* Updates the password of a already saved customer in the table `customers`
		*
		* @param 	Customer	$customer 	The customer
		*/
		public function updateCustomerPass($customer) {
			$this->database->connect();
			$sql = "UPDATE customers SET ";
			$sql .= 	"DateLastModified = NOW(), ";
			$sql .= 	"Pass = :pass ";
			$sql .= "WHERE CustomerID = :id";
			$this->database->runStatement($sql, [], $customer, "password");
			$this->database->close();
		}
		
		/*
		* Sets a already saved customer in the table `customers` to an admin
		*
		* @param 	Customer	$customer 	The customer
		*/
		public function updateCustomerAdminStatus($customer) {
			$this->database->connect();
			$sql = "UPDATE customers SET ";
			$sql .= 	"DateLastModified = NOW(), ";
			$sql .= 	"IsAdmin = '1'";
			$sql .= "WHERE CustomerID = :id";
			$this->database->runStatement($sql, [], $customer, "id");
			$this->database->close();
		}
		
		/*
		* Sets all not shown attributes of a customer of the table `customers` null
		*
		* @param 	Customer	$customer 	The customer
		*/
		public function deactivateCustomer($customer) {
			$this->database->connect();
			$sql = "UPDATE customers SET ";
			$sql .= 	"UserName = NULL, ";
			$sql .= 	"Pass = NULL, ";
			$sql .= 	"State = NULL, ";
			$sql .= 	"Address = NULL, ";
			$sql .= 	"Phone = NULL, ";
			$sql .= 	"Email = NULL, ";
			$sql .= 	"Privacy = 0, ";
			$sql .= 	"IsAdmin = 0 ";
			$sql .= "WHERE CustomerID = :id";
			$this->database->runStatement($sql, [], $customer, "id");
			$this->database->close();
		}
	
	}
	
?>