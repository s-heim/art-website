<?php
	require_once "../../processing/data/configuration/database.php";
	require_once "../../processing/data/models/subject.php";

	class SubjectsRepository {
		
		/**
		* @var database $database Saves the connection to the database
		*/	
		private $database;

		private function parse($data) {
			return Subject::getFromArray($data); 
		}

		private function parseMulti($data) {
			return DataHandler::getFromMultiArray(function($data) {
				return $this->parse($data);
			}, $data);
		}
		
		/*
		* Initializes the database object
		*/
		function __construct() {
			$this->database = new Database();
		}
		
		/*
		* Selects all subjects from the table `subjects`
		*
		* @return 	Subject[]
		*/
		public function getAllSubjects() {
			$this->database->connect();
			$sql = "SELECT SubjectID, SubjectName ";
			$sql .= "FROM subjects ";
			$sql .= "ORDER BY subjects.SubjectName ASC";
			$data = $this->database->runStatement($sql);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
		/*
		* Selects the subject with the given id from the table `subjects`
		*
		* @param 	int 	$id 		The id of the requestet subject
		* @return 	Subject
		*/
		function getSubjectByID($id) {
			$this->database->connect();
			$sql = "SELECT SubjectID, SubjectName ";
			$sql .= "FROM subjects ";
			$sql .= "WHERE subjects.SubjectID = :id";
			$data = $this->database->runStatement($sql, [ [":id", $id] ]);
			$this->database->close();
			return $this->parse($data[0]);
		}
		
		/*
		* Selects the subjects related to a given artwork from the table `subjects`
		*
		* @param 	int 	$id 		The id of the requestet artwork
		* @return 	Subject[]
		*/
		function getSubjectsByArtWork($id) {
			$this->database->connect();
			$sql = "SELECT subjects.SubjectID, subjects.SubjectName ";
			$sql .= "FROM subjects, artworksubjects ";
			$sql .= "WHERE artworksubjects.ArtWorkID = :id ";
			$sql .= "AND artworksubjects.SubjectID = subjects.SubjectID ";
			$sql .= "ORDER BY subjects.SubjectName ASC";
			$data = $this->database->runStatement($sql, [ [":id", $id] ]);
			$this->database->close();
			return $this->parseMulti($data);
			
		}
		
	}
	
?>