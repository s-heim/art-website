<?php
	require_once "../../processing/data/configuration/database.php";
	require_once "../../processing/data/models/gallery.php";
	
	class GalleriesRepository {
		
		/**
		* @var database $database Saves the connection to the database
		*/	
		private $database;

		private function parse($data) {
			return Gallery::getFromArray($data); 
		}

		private function parseMulti($data) {
			return DataHandler::getFromMultiArray(function($data) {
				return $this->parse($data);
			}, $data);
		}
		
		/*
		* Initializes the database object
		*/
		function __construct() {
			$this->database = new Database();
		}
		
		/*
		* Selects the gallery with the given id from the table `galleries`
		*
		* @param 	int 	$id 		The id of the requestet gallery
		* @return 	Gallery
		*/
		function getGallerieByID($id) {
			$this->database->connect();
			$sql = "SELECT GalleryID, GalleryName, GalleryNativeName, GalleryCity, GalleryCountry, Latitude, Longitude, GalleryWebSite ";
			$sql .= "FROM galleries ";
			$sql .= "WHERE galleries.GalleryID = :id";
			$data = $this->database->runStatement($sql, [ [":id", $id] ]);
			$this->database->close();
			if (count($data) < 1) {
				return $this->parse($data);
			}
			return $this->parse($data[0]);
		}
	
	}
	
?>