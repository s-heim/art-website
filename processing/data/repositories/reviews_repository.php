<?php
	require_once "../../processing/data/configuration/database.php";
	require_once "../../processing/data/models/review.php";

	class ReviewsRepository {
		
		/**
		* @var database $database Saves the connection to the database
		*/	
		private $database;

		private function parse($data) {
			return Review::getFromArray($data); 
		}

		private function parseMulti($data) {
			return DataHandler::getFromMultiArray(function($data) {
				return $this->parse($data);
			}, $data);
		}
		
		/*
		* Initializes the database object
		*/
		function __construct() {
			$this->database = new Database();
		}
		
		/*
		* Selects the review for a requested artwork from the table `reviews`
		*
		* @param 	int 	$id 		The id of the requestet artwork
		* @return 	Review[]
		*/
		public function getReviewsForArtwork($id) {
			$this->database->connect();
			$sql = "SELECT ReviewID, CustomerID, ArtWorkID, ReviewDate, Rating, Comment ";
			$sql .= "FROM reviews ";
			$sql .= "WHERE reviews.ArtWorkID =:id ";
			$sql .= "ORDER BY reviews.ReviewDate DESC";
			$data = $this->database->runStatement($sql, [ [":id", $id] ]);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
		/*
		* Selects the review for a requested artwork by a specified customer from the table `reviews`
		*select
		* @param 	int 	$artworkID 	The id of the requestet artwork
		* @param 	int 	$customerID	The id of the specified customer
		* @return 	Review
		*/
		public function getReviewForArtworkByCustomer($artworkID, $customerID) {
			$this->database->connect();
			$sql = "SELECT ReviewID, CustomerID, ArtWorkID, ReviewDate, Rating, Comment ";
			$sql .= "FROM reviews ";
			$sql .= "WHERE reviews.ArtWorkID = :artworkID ";
			$sql .= 	"AND reviews.CustomerID = :customerID ";
			$data = $this->database->runStatement($sql, [ [":artworkID", $artworkID],
															[":customerID", $customerID] ]);
			$this->database->close();
			if (count($data) < 1) {
				return $this->parse($data);
			}
			return $this->parse($data[0]);
		}
		
		/*
		* Orders all reviews descending by their insert date
		*
		* @param	int		$limit		The limit of reviews to get
		* @return 	Review[]
		*/
		public function getRecentReviews($limit=2) {
			$this->database->connect();
			$sql = "SELECT ReviewID, CustomerID, ArtWorkID, ReviewDate, Rating, Comment ";
			$sql .= "FROM reviews ";
			$sql .= "ORDER BY reviews.ReviewDate DESC ";
			$sql .= "Limit " . $limit;
			$data = $this->database->runStatement($sql);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
		/*
		* Adds a new review to the table `reviews`
		*
		* @param 	int		$artwork 	The id of the reviewed artwork
		* @param 	int		$customer 	The id of the reviewing customer
		* @param 	int		$rating 	The rating between 1 and 5 of the review
		* @param 	string	$comment 	The comment to the artwork
		*/
		public function addReview($artwork, $customer, $rating, $comment) {
			$this->database->connect();
			$sql = "INSERT INTO reviews ";
			$sql .= "(ArtWorkID, CustomerID, ReviewDate, Rating, Comment) ";
			$sql .= "VALUES (:artwork, :customer, NOW(), :rating, :comment)";
			$this->database->runStatement($sql, [ [":artwork", $artwork],
													[":customer", $customer],
													[":rating", $rating],
													[":comment", $comment] ]);
			$this->database->close();
		}
		
		/*
		* Deletes a review forever from the table `reviews`
		*
		* @param 	int 	$id 		The id of the review
		*/
		public function deleteReview($id) {
			$this->database->connect();
			$sql = "DELETE FROM reviews ";
			$sql .= "WHERE ReviewID = :id";
			$this->database->runStatement($sql, [ [":id", $id] ]);
			$this->database->close();
		}
	
	}
	
?>