<?php
	require_once "../../processing/data/configuration/database.php";
	require_once "../../processing/data/models/artwork.php";
	
	class ArtworksRepository {
		
		/**
		* @var database $database Saves the connection to the database
		*/	
		private $database;

		private function parse($data) {
			return Artwork::getFromArray($data); 
		}

		private function parseMulti($data) {
			return DataHandler::getFromMultiArray(function($data) {
				return $this->parse($data);
			}, $data);
		}
		
		
		/*
		* Initializes the database object
		*/
		function __construct() {
			$this->database = new Database();
		}
		
		/*
		* Selects all artworks from the table `artworks`
		*
		* @return 	Artwork[]
		*/
		public function getAllArtWorks() {
			$this->database->connect();
			$sql = "SELECT artworks.ArtWorkID, artworks.ArtistID, artworks.ImageFileName, artworks.Title, artworks.Description, artworks.Excerpt, artworks.YearOfWork ";
			$sql .= "FROM artworks, artists ";
			$sql .= "WHERE artworks.ArtistID = artists.ArtistID ";
			$sql .= "ORDER BY artists.LastName ASC, artists.FirstName ASC, artworks.Title ASC";
			$data = $this->database->runStatement($sql);
			$this->database->close();
			return $this->parseMulti($data);
		}
	
		/*
		* Selects the artwork with the given id from the table `artworks`
		*
		* @param 	int 	$id 		The id of the requestet artwork
		* @return 	Artwork
		*/
		function getArtWorkByID($id) {
			$this->database->connect();
			$sql = "SELECT ArtWorkID, ArtistID, ImageFileName, Title, Description, Excerpt, YearOfWork, Width, Height, Medium, GalleryID, MSRP, ArtWorkLink, GoogleLink ";
			$sql .= "FROM artworks ";
			$sql .= "WHERE artworks.ArtWorkID = :id";
			$data = $this->database->runStatement($sql, [ [":id", $id] ]);
			$this->database->close();
			if (count($data) < 1) {
				return $this->parse($data);
			}
			return $this->parse($data[0]);
		}
		
		/*
		* Selects the artworks with the given ids from the table `artworks`
		*
		* @param 	Int[] 	$id 		A list of ids of the requested artworks
		* @param 	string	$sortdir 	The direction in which the result shall be ordered (default: ascending)
		* @param 	string	$sortkey 	The key which after which the result shall be ordered (default: the id)
		* @return 	Artwork[]
		*/
		function getArtWorksByIDs($ids, $sortdir = "asc", $sortkey = "id") {
			if ($ids == null) {
				return null;
			}
			$this->database->connect();
			$sql = "SELECT ArtWorkID, ArtistID, ImageFileName, Title, Description, Excerpt, YearOfWork, Width, Height, Medium, GalleryID, MSRP, ArtWorkLink, GoogleLink ";
			$sql .= "FROM artworks ";
			$sql .= "WHERE ";
			foreach ($ids as $id) {
				$sql .= "artworks.ArtWorkID = ?";
				if (next($ids)) {
					$sql .= " OR ";
				}
			}
			switch($sortkey) {
				case("title"): 
					$sql .= "ORDER BY artworks.Title ";
					break;
				case("msrp"): 
					$sql .= "ORDER BY artworks.MSRP ";
					break;
				case("year"): 
					$sql .= "ORDER BY artworks.YearOfWork ";
					break;
				default: 
					$sql .= "ORDER BY artworks.ArtWorkID ";
					break;
			}
			if ($sortdir == "desc") {
				$sql .= "DESC";
			}
			else {
				$sql .= "ASC";
			}
			$data = $this->database->runStatement($sql, $ids);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
		/*
		* Selects the artworks from a requested artist by its id from the table `artworks`
		*
		* @param 	int 	$id 		The id of the requestet artist
		* @return 	Artwork[]
		*/
		function getArtWorksForArtist($artistID) {
			$this->database->connect();
			$sql = "SELECT ArtWorkID, ArtistID, ImageFileName, Title, YearOfWork ";
			$sql .= "FROM artworks ";
			$sql .= "WHERE ArtistID = :artistID ";
			$sql .= "ORDER BY ArtistID ASC, Title ASC";
			$data = $this->database->runStatement($sql, [ [":artistID", $artistID] ]);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
		/*
		* Selects the artworks from a requested genre by its id from the table `artworks`
		*
		* @param 	int 	$id 		The id of the requestet genre
		* @param 	string	$sortdir 	The direction in which the result shall be ordered (default: ascending)
		* @param 	string	$sortkey 	The key which after which the result shall be ordered (default: the id)
		* @return 	Artwork[]
		*/
		function getArtWorksForGenre($id, $sortdir = "asc", $sortkey = "id") {
			$this->database->connect();
			$sql = "SELECT artworks.ArtWorkID, artworks.ArtistID, artworks.ImageFileName, artworks.Title ";
			$sql .= "FROM artworks, artworkgenres ";
			$sql .= "WHERE artworks.ArtWorkID = artworkgenres.ArtWorkID ";
			$sql .=		"AND artworkgenres.GenreID = :id ";
			switch($sortkey) {
				case("title"): 
					$sql .= "ORDER BY artworks.Title ";
					break;
				case("msrp"): 
					$sql .= "ORDER BY artworks.MSRP ";
					break;
				case("year"): 
					$sql .= "ORDER BY artworks.YearOfWork ";
					break;
				default: 
					$sql .= "ORDER BY artworks.ArtWorkID ";
					break;
			}
			if ($sortdir == "desc") {
				$sql .= "DESC";
			}
			else {
				$sql .= "ASC";
			}
			$data = $this->database->runStatement($sql, [ [":id", $id] ]);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
		/*
		* Selects the artworks from a requested subject by its id from the table `artworks`
		*
		* @param 	int 	$id 		The id of the requestet subject
		* @param 	string	$sortdir 	The direction in which the result shall be ordered (default: ascending)
		* @param 	string	$sortkey 	The key which after which the result shall be ordered (default: the id)
		* @return 	Artwork[]
		*/
		function getArtWorksForSubject($id, $sortdir = "asc", $sortkey = "id") {
			$this->database->connect();
			$sql = "SELECT artworks.ArtWorkID, artworks.ArtistID, artworks.ImageFileName, artworks.Title, artworks.YearOfWork ";
			$sql .=	"FROM artworks, artworksubjects ";
			$sql .=	"WHERE artworks.ArtWorkID = artworksubjects.ArtWorkID ";
			$sql .=		"AND artworksubjects.SubjectID = :id ";
			switch($sortkey) {
				case("title"): 
					$sql .= "ORDER BY artworks.Title ";
					break;
				case("msrp"): 
					$sql .= "ORDER BY artworks.MSRP ";
					break;
				case("year"): 
					$sql .= "ORDER BY artworks.YearOfWork ";
					break;
				default: 
					$sql .= "ORDER BY artworks.ArtWorkID ";
					break;
			}
			if ($sortdir == "desc") {
				$sql .= "DESC";
			}
			else {
				$sql .= "ASC";
			}
			$data = $this->database->runStatement($sql, [ [":id", $id] ]);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
		/*
		* Does a wild card search over the `Title` Column of the table `artworks`
		*
		* @param 	string	$search 	The search value that shall be part of the titles
		* @param 	string	$sortdir 	The direction in which the result shall be ordered (default: ascending)
		* @param 	string	$sortkey 	The key which after which the result shall be ordered (default: the id)
		* @return 	Artwork[]
		*/
		function simpleSearchArtworks($search, $sortdir = "asc", $sortkey = "id") {
			$search = "%" . $search . "%";
			$this->database->connect();
			$sql = "SELECT ArtWorkID, Title, ImageFileName, ArtistID, YearOfWork ";
			$sql .= "FROM artworks ";
			$sql .= "WHERE artworks.Title LIKE :search ";
			switch($sortkey) {
				case("title"): 
					$sql .= "ORDER BY artworks.Title ";
					break;
				case("msrp"): 
					$sql .= "ORDER BY artworks.MSRP ";
					break;
				case("year"): 
					$sql .= "ORDER BY artworks.YearOfWork ";
					break;
				default: 
					$sql .= "ORDER BY artworks.ArtWorkID ";
					break;
			}
			if ($sortdir == "desc") {
				$sql .= "DESC";
			}
			else {
				$sql .= "ASC";
			}
			$data = $this->database->runStatement($sql, [ [":search", $search] ]);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
		/*
		* Does a specified, advanced search over the table `artworks`
		*	
		* @param 	string	$title 			The search value that shall be part of the titles (optional)
		* @param 	int		$minYearOfWork	The minimum of the range when the artworks shall have been created (optional)
		* @param 	int		$maxYearOfWork	The maximum of the range when the artworks shall have been created (optional)
		* @param 	string	$genre 			The genre that shall be related to the artworks (optional)
		* @param 	string	$subject 		The subject that shall be related to the artworks (optional)
		* @param 	string	$sortdir 		The direction in which the result shall be ordered (default: ascending)
		* @param 	string	$sortkey 		The key which after which the result shall be ordered (default: the id)
		* @return 	Artwork[]
		*/
		function advancedSearchArtworks($title=null, $minYearOfWork=null, $maxYearOfWork=null, $genre=null, $subject=null, $sortdir = "asc", $sortkey = "id") {
			$title = "%" . $title . "%";
			$this->database->connect();
			$sql = "SELECT DISTINCT artworks.ArtWorkID, artworks.Title, artworks.ImageFileName, artworks.ArtistID, artworks.YearOfWork ";
			$sql .= "FROM artworks, artworkgenres, genres, artworksubjects, subjects ";
			$sql .= "WHERE artworks.Title LIKE :title ";
			if ($minYearOfWork != null) {
				$sql .= "AND artworks.YearOfWork >= :minYearOfWork ";
			}
			if ($maxYearOfWork != null) {
				$sql .= "AND artworks.YearOfWork <= :maxYearOfWork ";
			}
			if ($genre != null) {
				$sql .= "AND artworks.ArtWorkID = artworkgenres.ArtWorkID ";
				$sql .= "AND genres.GenreID = artworkgenres.GenreID ";
				$sql .= "AND genres.GenreName = :genre ";
			}
			if ($subject != null) {
				$sql .= "AND artworks.ArtWorkID = artworksubjects.ArtWorkID ";
				$sql .= "AND subjects.SubjectID = artworksubjects.SubjectID ";
				$sql .= "AND subjects.SubjectName = :subject ";
			}
			switch($sortkey) {
				case("title"): 
					$sql .= "ORDER BY artworks.Title ";
					break;
				case("msrp"): 
					$sql .= "ORDER BY artworks.MSRP ";
					break;
				case("year"): 
					$sql .= "ORDER BY artworks.YearOfWork ";
					break;
				default: 
					$sql .= "ORDER BY artworks.ArtWorkID ";
					break;
			}
			if ($sortdir == "desc") {
				$sql .= "DESC";
			}
			else {
				$sql .= "ASC";
			}
			$data = $this->database->runStatement($sql, [ [":title", $title], 
															[":minYearOfWork", $minYearOfWork], 
															[":maxYearOfWork", $maxYearOfWork], 
															[":genre", $genre], 
															[":subject", $subject]]);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
		/*
		* Gives the average rating of a artwork
		*
		* @param 	int 	$id 		The id of the requestet artwork
		* @return 	Artwork
		*/
		public function getArtworkRatingByID($id) {
			$this->database->connect();
			$sql = "SELECT ArtWorkID, ROUND(AVG(reviews.Rating), 2) AS Ratings, COUNT(reviews.Rating) AS NumberOfReviews ";
			$sql .= "FROM reviews ";
			$sql .= "WHERE ArtWorkID = :id";
			$data = $this->database->runStatement($sql, [ [":id", $id] ]);
			$this->database->close();
			return $data;
		}
		
		/*
		* Orders all artworks by their average rating
		*
		* @param	int		$limit		The limit of artworks to get
		* @return 	String[]
		*/
		public function getTopArtworks($limit=3) {
			$this->database->connect();
			$sql = "SELECT ArtWorkID, AVG(Rating) AS AvgRating ";
			$sql .=	"FROM reviews ";
			$sql .= "GROUP BY ArtWorkID ";
			$sql .= "ORDER BY AVG(Rating) DESC, SUM(Rating) DESC ";
			$sql .= "Limit " . $limit;
			$data = $this->database->runStatement($sql);
			$this->database->close();
			return $data;
		}
		
		/*
		* Orders all artworks descending by their insert order
		*
		* @param	int		$limit		The limit of artworks to get
		* @return 	String[]
		*/
		public function getNewestArtworks($limit=3) {
			$this->database->connect();
			$sql = "SELECT ArtWorkID, Title, ImageFileName, ArtistID  ";
			$sql .= "FROM artworks ";
			$sql .= "ORDER BY artworks.ArtWorkID DESC ";
			$sql .= "Limit " . $limit;
			$data = $this->database->runStatement($sql);
			$this->database->close();
			return $data;
		}
			
	}
	
?>