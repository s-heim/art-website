<?php
	require_once "../../processing/data/configuration/database.php";
	require_once "../../processing/data/models/genre.php";
	
	class GenresRepository {
		
		/**
		* @var database $database Saves the connection to the database
		*/	
		private $database;

		private function parse($data) {
			return Genre::getFromArray($data); 
		}

		private function parseMulti($data) {
			return DataHandler::getFromMultiArray(function($data) {
				return $this->parse($data);
			}, $data);
		}
		
		/*
		* Initializes the database object
		*/
		function __construct() {
			$this->database = new Database();
		}
		
		/*
		* Selects all genres from the table `genres`
		*
		* @param 	string 	$order		Specification about the sort order (optional)
		* @return 	Genre[]
		*/
		public function getAllGenres($order=null) {
			$this->database->connect();
			$sql = "SELECT GenreID, GenreName ";
			$sql .= "FROM genres ";
			switch($order) {
				case ("name"): 
					$sql .= "ORDER BY GenreName ASC";
					break;
				default: 
					$sql .= "ORDER BY Era ASC, GenreName ASC";
					break;
			}
			$data = $this->database->runStatement($sql);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
		/*
		* Selects the genre with the given id from the table `genres`
		*
		* @param 	int 	$id 		The id of the requestet genre
		* @return 	Genre
		*/
		function getGenreByID($id) {
			$this->database->connect();
			$sql = "SELECT genres.GenreID, genres.GenreName, genres.Description, genres.Link ";
			$sql .= "FROM genres ";
			$sql .= "WHERE genres.GenreID = :id";
			$data = $this->database->runStatement($sql, [ [":id", $id] ]);
			$this->database->close();
			return $this->parse($data[0]);
		}
		
		/*
		* Selects the genres related to a given artwork from the table `genres`
		*
		* @param 	int 	$id 		The id of the requestet artwork
		* @return 	Genre[]
		*/
		function getGenresByArtWork($id) {
			$this->database->connect();
			$sql = "SELECT genres.GenreID, genres.GenreName ";
			$sql .= "FROM genres, artworkgenres ";
			$sql .= "WHERE artworkgenres.ArtWorkID = :id ";
			$sql .= 	"AND artworkgenres.GenreID = genres.GenreID ";
			$sql .= "ORDER BY genres.Era ASC, genres.GenreName ASC";
			$data = $this->database->runStatement($sql, [ [":id", $id] ]);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
	}
	
?>