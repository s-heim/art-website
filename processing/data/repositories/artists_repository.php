<?php
	require_once "../../processing/data/configuration/database.php";
    require_once "../../processing/data/models/artist.php";
	
	class ArtistsRepository {	

        /**
		* @var database $database Saves the connection to the database
		*/	
		private $database;

		private function parse($data) {
			return Artist::getFromArray($data); 
		}

		private function parseMulti($data) {
			return DataHandler::getFromMultiArray(function($data) {
				return $this->parse($data);
			}, $data);
		}
		
        /*
		* Initializes the database object
		*/
		function __construct() {
			$this->database = new Database();
		}
		
        /*
		* Selects all artists from the table `artists`
		*
		* @return 	Artist[]
		*/
		public function getAllArtists() {
			$this->database->connect();
			$sql = "SELECT ArtistID, FirstName, LastName ";
			$sql .= "FROM artists ";
			$sql .= "ORDER BY artists.LastName ASC, artists.FirstName ASC";
			$data = $this->database->runStatement($sql);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
        /*
		* Selects the artist with the given id from the table `artists`
		*
		* @param 	int 	$id 		The id of the requestet artist
		* @return 	Artist[]
		*/
		function getArtistByID($id) {
			$this->database->connect();
			$sql = "SELECT ArtistID, FirstName, LastName, Nationality, YearOfBirth, YearOfDeath, Details, ArtistLink ";
			$sql .= "FROM artists ";
			$sql .= "WHERE artists.ArtistID = :id";
			$data = $this->database->runStatement($sql, [ [":id", $id]]);
			$this->database->close();
			if (count($data) < 1) {
				return $this->parse($data);
			}
			return $this->parse($data[0]);
		}
		
        /*
		* Selects the artists with the given ids from the table `artists`
		*
		* @param 	int[] 	$id 		A list of ids of the requested artists
		* @param 	string	$sortdir 	The direction in which the result shall be ordered (default: ascending)
		* @param 	string	$sortkey 	The key which after which the result shall be ordered (default: the id)
		* @return 	Artist[]
		*/
		function getArtistsByIDs($ids, $sortdir = "asc", $sortkey = "id") {
			if ($ids == null) {
				return null;
			}
			$this->database->connect();
			$sql = "SELECT ArtistID, FirstName, LastName ";
			$sql .= "FROM artists ";
			$sql .= "WHERE ";
			foreach ($ids as $id) {
				$sql .= "artists.ArtistID = ?";
				if (next($ids)) {
					$sql .= " OR ";
				}
			}
			switch($sortkey) {
				case("last name"): 
					$sql .= " ORDER BY artists.LastName ";
					break;
				default: 
					$sql .= " ORDER BY artists.ArtistID ";
					break;
			}
			if ($sortdir == "desc") {
				$sql .= "DESC";
			}
			else {
				$sql .= "ASC";
			}
			$data = $this->database->runStatement($sql, $ids);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
        /*
		* Does a wild card search over the `LastName` Column of the table `artists`
		*
		* @param 	string	$search		The search value that shall be part of the last name
		* @param 	string	$sortdir	The direction in which the result shall be ordered (default: ascending)
		* @param 	string	$sortkey 	The key which after which the result shall be ordered (default: the id)
		* @return 	Artist[]
		*/
		function simpleSearchArtists($search, $sortdir = "asc", $sortkey = "id") {
			$search = "%" . $search . "%";
			$this->database->connect();
			$sql = "SELECT ArtistID, FirstName, LastName ";
			$sql .= "FROM artists ";
			$sql .= "WHERE artists.LastName LIKE :search ";
			switch($sortkey) {
				case("last name"): 
					$sql .= "ORDER BY artists.LastName ";
					break;
				default: 
					$sql .= "ORDER BY artists.ArtistID ";
					break;
			}
			if ($sortdir == "desc") {
				$sql .= "DESC";
			}
			else {
				$sql .= "ASC";
			}
			$data = $this->database->runStatement($sql, [ [":search", $search]]);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
        /*
		* Does a specified, advanced search over the table `artists`
		*	
		* @param 	string	$lastname			The search value that shall be part of the last name (optional)
		* @param 	string	$firstname			The search value that shall be part of the first name (optional)
		* @param 	int		$minYearOfLiving	The minimum of the range when the artist shall have lived (optional)
		* @param 	int		$maxYearOfLiving	The maximum of the range when the artist shall have lived (optional)
		* @param 	string	$nationality		The nationality of the artists (optional)
		* @param 	string	$sortdir			The direction in which the result shall be ordered (default: ascending)
		* @param 	string	$sortkey 			The key which after which the result shall be ordered (default: the id)
		* @return 	Artist[]
		*/
		function advancedSearchArtists($lastname=null, $firstname=null, $minYearOfLiving=null, $maxYearOfLiving=null, $nationality=null, $sortdir = "asc", $sortkey = "id") {
			$lastname = "%" . $lastname . "%";
			$firstname = "%" . $firstname . "%";
			$this->database->connect();
			$sql = "SELECT ArtistID, FirstName, LastName ";
			$sql .= "FROM artists ";
			$sql .= "WHERE artists.LastName LIKE :lastname ";
			$sql .= "AND artists.FirstName LIKE :firstname ";
			if ($minYearOfLiving != null) {
				$sql .= "AND artists.YearOfDeath >= :minYearOfLiving ";
			}
			if ($maxYearOfLiving != null) {
				$sql .= "AND artists.YearOfBirth <= :maxYearOfLiving ";
			}
			if ($nationality != null) {
				$sql .= "AND artists.Nationality = :nationality ";
			}
			switch($sortkey) {
				case("last name"): 
					$sql .= "ORDER BY artists.LastName ";
					break;
				default: 
					$sql .= "ORDER BY artists.ArtistID ";
					break;
			}
			if ($sortdir == "desc") {
				$sql .= "DESC";
			}
			else {
				$sql .= "ASC";
			}
			$data = $this->database->runStatement($sql, [ [":lastname", $lastname], 
															[":firstname", $firstname], 
															[":minYearOfLiving", $minYearOfLiving], 
															[":maxYearOfLiving", $maxYearOfLiving], 
															[":nationality", $nationality]]);
			$this->database->close();
			return $this->parseMulti($data);
		}
		
        /*
		* Gives the average rating of art by a given artist
		*
		* @param 	int 	$id 		The id of the requestet artist
		* @return 	String[]
		*/
		public function getArtistRatingByID($id) {
			$this->database->connect();
			$sql = "SELECT artworks.ArtistID, ROUND(AVG(reviews.Rating), 2) AS Ratings ";
			$sql .= "FROM reviews, artworks ";
			$sql .= "WHERE reviews.ArtWorkID = artworks.ArtWorkID ";
			$sql .=		"AND artworks.ArtistID = :id ";
			$sql .= "GROUP BY artworks.ArtistID ";
			$sql .= "ORDER BY AVG(reviews.Rating) DESC, SUM(reviews.Rating) DESC";
			$data = $this->database->runStatement($sql, [ [":id", $id]]);
			$this->database->close();
			return $data;
		}
		
        /*
		* Gets the ids of the artists with the best rating
		*
		* @param	int		$limit		The limit of artists to get
		* @return 	string[]
		*/
		public function getTopArtists($limit=3) {
			$this->database->connect();
			$sql = "SELECT artworks.ArtistID, AVG(reviews.Rating) AS AvgRating, SUM(reviews.Rating) AS SumRating ";
			$sql .= "FROM reviews, artworks ";
			$sql .= "WHERE reviews.ArtWorkID = artworks.ArtWorkID ";
			$sql .= "GROUP BY artworks.ArtistID ";
			$sql .= "ORDER BY AVG(reviews.Rating) DESC, SUM(reviews.Rating) DESC ";
			$sql .= "Limit " . $limit;
			$data = $this->database->runStatement($sql);
			$this->database->close();
			return $data;
		}
		
        /*
		* Selects all nationalities from the table `artists`
		*
		* @return 	String[]
		*/
		public function getAllNationalities() {
			$this->database->connect();
			$sql = "SELECT DISTINCT Nationality ";
			$sql .= "FROM artists ";
			$sql .= "ORDER BY Nationality ASC";
			$data = $this->database->runStatement($sql);
			$this->database->close();
			return $data;
		}
		
	}
	
?>