<?php
    class DataHandler {

        /*
		* Creates an array of entities from an multidimensional array
        * 
        * @param 	func    	$getFromArrayFunc 	The function for any single entry to apply
        * @param 	string[]	$array 		        The data as (multi-)array
        * @return   T[]
		*/
        static function getFromMultiArray($getFromArrayFunc, $array) {
            for ($i = 0; $i < count($array); $i++) {
                $array[$i] = $getFromArrayFunc($array[$i]);
            }
            return $array;
        }

        static function getEntryOrNull($key, $array) {
            if (array_key_exists($key, $array)) {
                return $array[$key];
            }

            return null;
        }

        /*
		* Creates the path to a image in a given size
		* 
		* @param 	string		$filename	The file name of the picture
		* @param 	string		$type		The category of the picture (artist, genre, subject, work)
		* @param 	string		$size		The size of the picture
		* @return 	string
		*/
		static function getPicturePath($filename, $type, $size) {
			$imgsrc = "../../img/" . $type . '/' . $size . '/' . $filename . '.jpg';
			if (file_exists($imgsrc)) {
				return $imgsrc;
			}
			else {
				$imgsrc = "../../img/layout/" . $type . '-' . $size . '.jpg';
				return $imgsrc;
			}
		}

        /*
		* Cuts a link in front and end to show it shorter
		* 
		* @param 	string		$link		The link to cut
		* @return 	string
		*/
		static function cutLink($link) {
			$link = substr($link, strpos($link, "/") + 2);
			$link = substr($link, 0, strpos($link, "/"));
			return $link;
		}

    }
?>

