<?php
    require_once "../../processing/data/models/data_handler.php";

    class Subject {

        private $id;
        private $name;
        private $artworks = array();

        /*
		* Initializes the subject object
        * 
        * @param 	int 	$id 		The id of the subject
        * @param 	string 	$name   	The name
		*/
        function __construct($id, $name) {
            $this->id = intval($id);
            $this->name = $name;
        }

        /*
		* Adds an artwork reference to this subject
        * 
        * @param 	Artwork	$artwork 		The artwork object
		*/
        function addArtwork($artwork) {
            $this->artworks[count($this->artworks)] = $artwork;
        }

        /*
		* Adds an array of artworks to this subject
        * 
        * @param 	Artwork[]	$artworks 		An array of artwork objects
		*/
        function addArtworks($artworks) {
            foreach($artworks as $artwork) {
                $this->addArtwork($artwork);
            }
        }

        function getName() {
            return $this->name;
        }

        function getID() {
            return $this->id;
        }

        function getPicture($size="square-thumb") {
            return DataHandler::getPicturePath($this->id, "subjects", $size);
        }

        /*
		* Creates HTML-text for representation of this subject
        * 
        * @return   string
		*/
        function getShortDetails() {
			$picture = DataHandler::getPicturePath($this->id, "subjects", "square-thumbs");
			$box = "<a href='../single/subject.php?id=" . $this->id . "' class='card-link'>";
			$box .= "<button class='btn btn-light btn-block mt-3'>";
			$box .= "<img src='" . $picture . "' alt='Sample art of " . $this->name . "' />";
			$box .= "<br />";
			$box .= $this->name;
			$box .= "</button>";
			$box .= "</a>";
			return $box;
		}

        /*
		* Creates a subject from an associative array
        * 
        * @param 	string[]	$array 		The data as array
        * @return   Subject
		*/
        static function getFromArray($array) {
            return new Subject(DataHandler::getEntryOrNull('SubjectID', $array), 
                                DataHandler::getEntryOrNull('SubjectName', $array));
        }

    }

?>