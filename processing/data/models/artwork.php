<?php
    require_once "../../processing/data/models/data_handler.php";

    class Artwork {

        private $id;
        private $artistID;
        private $artist;
        private $imageFileName;
        private $title;
        private $description;
        private $excerpt;
        private $type;
        private $yearOfWork;
        private $width;
        private $height;
        private $medium;
        private $originalHome;
        private $galleryID;
        private $gallery;
        private $cost;
        private $msrp;
        private $workLink;
        private $googleLink;
        private $rating;
        private $votes;
        private $genres = array();
        private $subjects = array();
        private $reviews = array();

        /*
		* Initializes the artwork object
        * 
        * @param 	int 	$id 		The id of the subject
        * @param 	string 	$name   	The name
		*/
        function __construct($id, $artistID, $imageFileName, $title, $description, $excerpt, $type, $yearOfWork, $width, $height, $medium, $originalHome, $galleryID, $cost, $msrp, $workLink, $googleLink) {
            $this->id = intval($id);
            $this->artistID = intval($artistID);
            $this->imageFileName = $imageFileName;
            $this->title = $title;
            $this->description = $description;
            $this->excerpt = $excerpt;
            $this->type = $type;
            $this->yearOfWork = $yearOfWork;
            $this->width = intval($width);
            $this->height = intval($height);
            $this->medium = $medium;
            $this->originalHome = $originalHome;
            $this->galleryID = intval($galleryID);
            $this->cost = doubleval($cost);
            $this->msrp = doubleval($msrp);
            $this->workLink = $workLink;
            $this->googleLink = $googleLink;

            if (strlen($this->imageFileName) == 5) {
				$this->imageFileName = "0" . $this->imageFileName;
			}
        }

        function getID() {
            return $this->id;
        }

        function getArtistID() {
            return $this->artistID;
        }

        function getGalleryID() {
            return $this->galleryID;
        }

        function getTitle() {
            return $this->title;
        }

        function getGallery() {
            return $this->gallery;
        }

        function getGalleryDetails() {
            $box = "<tr><td scope='row'>Name:</td><td>" . $this->gallery->getName() . "</td></tr>";

            if ($this->gallery->getNativeName() != null) {
                $box .= "<tr><td scope='row'>Native Name:</td><td>" . $this->gallery->getNativeName() . "</td></tr>";
            }

            if ($this->gallery->getLocation() != null) {
                $box .= "<tr><td scope='row'>Location:</td><td>" . $this->gallery->getLocation() . "</td></tr>";
            }

            if ($this->gallery->getLink() != null) {
                $box .= "<tr><td scope='row'>Website:</td><td>" . $this->gallery->getLink() . "</td></tr>";
            }

            return $box;
        }

        function getArtist() {
            return $this->artist;
        }

        function getYear() {
            return $this->yearOfWork;
        }

        function getPicture($size="medium") {
            return DataHandler::getPicturePath($this->imageFileName, "works", $size);
        }

        function getDimensions() {
            return $this->width . "cm x " . $this->height . "cm";
        }

        function getMedium() {
            return $this->medium;
        }

        function getDescription() {
            return $this->description;
        }

        function getExcerpt() {
            return $this->excerpt;
        }

        function getPrice() {
            return "$" . (int) $this->msrp . ".00";
        }

        function getLink() {
            $link = "";
			if ($this->workLink != null) {
				$link .= "<a href='" . $this->workLink . "'  class='card-link' target='_blank'>";
				$link .= DataHandler::cutLink($this->workLink);
				$link .= "</a>";
				if ($this->googleLink != null) {
					$link .= "<br />";
				}
			}
			if ($this->googleLink != null) {
				$link .= "<a href='" . $this->googleLink . "'  class='card-link' target='_blank'>";
				$link .= DataHandler::cutLink($this->googleLink);
				$link .= "</a>";
			}

            if (strlen($link) == 0) {
                $link = "-";
            }

            return $link;
        }

        function getGenres() {
            $box = "";
            foreach ($this->genres as $genre) {
				$box .= "<a href='../single/genre.php?id=" . $genre->getID() . "' class='card-link'>";
				$box .= $genre->getName();
				$box .= "</a>";
				if (next($this->genres)) {
					$box .= "<br />";
				}
			}
            return $box;
        }

        function getSubjects() {
            $box = "";
            foreach ($this->subjects as $subject) {
				$box .= "<a href='../single/subject.php?id=" . $subject->getID() . "' class='card-link'>";
				$box .= $subject->getName();
				$box .= "</a>";
				if (next($this->subjects)) {
					$box .= "<br />";
				}
			}
            return $box;
        }

        function getRating() {
            return number_format($this->rating, 2, '.', '');
        }

        function getVotes() {
            return $this->votes;
        }

        function setRating($rating, $votes) {
            $this->rating = doubleval($rating);
            $this->votes = intval($rating);
        }

        function setArtist($artist) {
            if ($artist->getID() == $this->artistID) {
                $this->artist = $artist;
            }
        }

        function setGallery($gallery) {
            if ($gallery->getID() == $this->galleryID) {
                $this->gallery = $gallery;
            }
        }

        /*
		* Adds a genre reference to this artwork
        * 
        * @param 	Genre	$genre 		The genre object
		*/
        function addGenre($genre) {
            $this->genres[count($this->genres)] = $genre;
        }

        /*
		* Adds a subject reference to this artwork
        * 
        * @param 	Subject	$subject 		The subject object
		*/
        function addSubject($subject) {
            $this->subjects[count($this->subjects)] = $subject;
        }

        /*
		* Adds a review reference to this artwork
        * 
        * @param 	Review	$review 		The review object
		*/
        function addReview($review) {
            $this->reviews[count($this->reviews)] = $review;
        }

        /*
		* Adds an array of genres to this artwork
        * 
        * @param 	Genre[]	$genres 		An array of genre objects
		*/
        function addGenres($genres) {
            foreach($genres as $genre) {
                $this->addGenre($genre);
            }
        }

        /*
		* Adds an array of subjects to this artwork
        * 
        * @param 	Subject[]	$subjects 		An array of subject objects
		*/
        function addSubjects($subjects) {
            foreach($subjects as $subject) {
                $this->addSubject($subject);
            }
        }

        /*
		* Adds an array of reviews to this artwork
        * 
        * @param 	Review[]	$reviews 		An array of review objects
		*/
        function addReviews($reviews) {
            foreach($reviews as $review) {
                $this->addGenre($review);
            }
        }


        /*
		* Creates HTML-text for representation of this artwork
        * 
        * @param 	string		$extend			Specifications whether the artist, year or both shall be shown.
		* @param 	int			$rating			The average rating of the artwork (if needed)
        * @return   string
		*/
        function getShortDetails($extend, $withRating=null) {
			$picture = DataHandler::getPicturePath($this->imageFileName, "works", "square-small");
			$box = "<a href='../single/artwork.php?id=" . $this->id . "' class='card-link'>";
			$box .= "<button class='btn btn-light btn-block mt-3'>";
			$box .= "<img src='" . $picture . "' alt='Artwork Image: " . $this->title . "' />";
			$box .= "<br />";
			$box .= $this->title;
			if ($extend == "artist") {	
				$box .= "<br />";
				$box .= "by ";
				$box .= "<span class='font-italic'>";
				$box .= $this->artist->getName();
				$box .= "</span>";
			}
			else if ($extend == "year") {
				$box .= " (" . $this->yearOfWork . ")";
			}
			else {
				$box .= " (" . $this->yearOfWork . ")";
				$box .= "<br />";
				$box .= "by ";
				$box .= "<span class='font-italic'>";
				$box .= $this->artist->getName();
				$box .= "</span>";
			}
			if (isset($withRating)) {
				$box .= "<br />";
				$box .= "<small>";
				$box .= "Ø ";
				$box .= $withRating;
				$box .= " of 5.00 stars";
				$box .= "</small>";
			}
			$box .= "</button>";
			$box .= "</a>";
			return $box;
		}

        /*
		* Creates an artwork from an associative array
        * 
        * @param 	string[]	$array 		The data as array
        * @return   Artwork
		*/
        static function getFromArray($array) {
            return new Artwork(DataHandler::getEntryOrNull('ArtWorkID', $array), 
                                DataHandler::getEntryOrNull('ArtistID', $array), 
                                DataHandler::getEntryOrNull('ImageFileName', $array), 
                                DataHandler::getEntryOrNull('Title', $array), 
                                DataHandler::getEntryOrNull('Description', $array), 
                                DataHandler::getEntryOrNull('Excerpt', $array), 
                                DataHandler::getEntryOrNull('ArtWorkType', $array), 
                                DataHandler::getEntryOrNull('YearOfWork', $array), 
                                DataHandler::getEntryOrNull('Width', $array), 
                                DataHandler::getEntryOrNull('Height', $array), 
                                DataHandler::getEntryOrNull('Medium', $array), 
                                DataHandler::getEntryOrNull('OriginalHome', $array), 
                                DataHandler::getEntryOrNull('GalleryID', $array), 
                                DataHandler::getEntryOrNull('Cost', $array), 
                                DataHandler::getEntryOrNull('MSRP', $array), 
                                DataHandler::getEntryOrNull('ArtWorkLink', $array), 
                                DataHandler::getEntryOrNull('GoogleLink', $array));
        }

    }

?>