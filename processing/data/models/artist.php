<?php
    require_once "../../processing/data/models/data_handler.php";
    
    class Artist {

        private $id;
        private $firstName;
        private $lastName;
        private $nationality;
        private $yearOfBirth;
        private $yearOfDeath;
        private $details;
        private $link;
        private $artworks = array();

        /*
		* Initializes the artist object
        * 
        * @param 	int 	$id 		    The id of the artist
        * @param 	string 	$firstName 		The first name
        * @param 	string 	$lastName 		The last name
        * @param 	string 	$nationality	The nationality
        * @param 	int 	$yearOfBirth 	The year of birth
        * @param 	int 	$yearOfDeath	The year of death
        * @param 	string 	$details	    More details of the artist as text
        * @param 	string 	$link	        A link to more information of the artist
		*/
        function __construct($id, $firstName, $lastName, $nationality, $yearOfBirth, $yearOfDeath, $details, $link) {
            $this->id = intval($id);
            $this->firstName = $firstName;
            $this->lastName = $lastName;
            $this->nationality = $nationality;
            $this->yearOfBirth = intval($yearOfBirth);
            $this->yearOfDeath = intval($yearOfDeath);
            $this->details = $details;
            $this->link = $link;
        }

        /*
		* Adds an artwork reference to this artist
        * 
        * @param 	Artwork	$artwork 		The artwork object
		*/
        function addArtwork($artwork) {
            $this->artworks[count($this->artworks)] = $artwork;
        }

        /*
		* Adds an array of artworks to this artist
        * 
        * @param 	Artwork[]	$artworks 		An array of artwork objects
		*/
        function addArtworks($artworks) {
            foreach($artworks as $artwork) {
                $this->addArtwork($artwork);
            }
        }

        /*
		* Adds the first and last name when both available
        * 
        * @param 	boolean	$withReplace 		Determines whether '-signs shall be eliminated
        * @return   string
		*/
        function getName($withReplace=false) {
            $name = null;
            if (isset($this->firstName)) {
                $name = $this->firstName . " " . $this->lastName;
			}
            else {
                $name = $this->lastName;
            }
			
            if ($withReplace) {
                return str_replace("'", "", $name);
            }

            return $name;
        }

        function getPicture($size="square-thumb") {
            return DataHandler::getPicturePath($this->id, "artists", $size);
        }

        function getID() {
            return $this->id;
        }

        function getDetails() {
            return $this->details;
        }

        function getNationality() {
            return $this->nationality;
        }

        function getLink() {
            return "<a href='" . $this->link . "' class='card-link' target='_blank'>" . DataHandler::cutLink($this->link) . "</a>";
        }

        function getDates() {
            return $this->yearOfBirth . " - " . $this->yearOfDeath;
        }

        function getArtWorks() {
            return $this->artworks;
        }

        /*
		* Creates HTML-text for representation of this artist
        * 
        * @param 	string	$rating 		The average rating of this artist
        * @return   string
		*/
        function getShortDetails($rating=null) {
			$picture = DataHandler::getPicturePath($this->id, "artists", "square-thumb");
			$box = "<a href='../single/artist.php?id=" . $this->id . "' class='card-link'>";
			$box .= "<button class='btn btn-light btn-block mt-3'>";
			$box .= "<img src='" . $picture . "' alt='Portrait of " .  $this->getName(true) . "' />";
			$box .= "<br />";
			$box .= $this->getName();
			if (isset($rating)) {
				$box .= "<br />";
				$box .= "<small>";
				$box .= "Ø ";
				$box .= number_format((float)$rating, 2, '.', '');
				$box .= " of 5.00 stars";
				$box .= "</small>";
			}
			$box .= "</button>";
			$box .= "</a>";
			return $box;
		}

        /*
		* Creates an artist from an associative array
        * 
        * @param 	string[]	$array 		The data as array
        * @return   Artist
		*/
        static function getFromArray($array) {
            return new Artist(DataHandler::getEntryOrNull('ArtistID', $array), 
                                DataHandler::getEntryOrNull('FirstName', $array), 
                                DataHandler::getEntryOrNull('LastName', $array), 
                                DataHandler::getEntryOrNull('Nationality', $array), 
                                DataHandler::getEntryOrNull('YearOfBirth', $array), 
                                DataHandler::getEntryOrNull('YearOfDeath', $array), 
                                DataHandler::getEntryOrNull('Details', $array), 
                                DataHandler::getEntryOrNull('ArtistLink', $array));
        }
    }

?>