<?php
    require_once "../../processing/data/models/data_handler.php";

    class Gallery {

        private $id;
        private $name;
        private $nativeName;
        private $city;
        private $country;
        private $latitude;
        private $longitude;
        private $link;
        private $artworks = array();

        /*
		* Initializes the gallery object
        * 
        * @param 	int 	$id 		The id of the gallery
        * @param 	string 	$name 	    The (current) name
        * @param 	string 	$nativeName The native Name
        * @param 	string 	$city   	The city
        * @param 	string 	$country    The country
        * @param 	double 	$latitude   The latitude
        * @param 	double 	$longitude  The longitude
        * @param 	string 	$link   	The link to the website
		*/
        function __construct($id, $name, $nativeName, $city, $country, $latitude, $longitude, $link) {
            $this->id = intval($id);
            $this->name = $name;
            $this->nativeName = $nativeName;
            $this->city = $city;
            $this->country = $country;
            $this->latitude = doubleval($latitude);
            $this->longitude = doubleval($longitude);
            $this->link = $link;
        }

        /*
		* Adds an artwork reference to this gallery
        * 
        * @param 	Artwork	$artwork 		The artwork object
		*/
        function addArtwork($artwork) {
            $this->artworks[count($this->artworks)] = $artwork;
        }

        /*
		* Adds an array of artworks to this gallery
        * 
        * @param 	Artwork[]	$artworks 		An array of artwork objects
		*/
        function addArtworks($artworks) {
            foreach($artworks as $artwork) {
                $this->addArtwork($artwork);
            }
        }

        function getName() {
            return $this->name;
        }

        function getID() {
            return $this->id;
        }

        function getNativeName() {
            if ($this->name != $this->nativeName) {
                return $this->nativeName;
            }

            return null;
        }

        function getLocation() {
            if ($this->city != null && $this->country != null) {
                return $this->city . " (" . $this->country . ")";
            }
            else if ($this->city != null) {
                return $this->city;
            }
            else if ($this->country != null) {
                return $this->country;
            }

            return null;
        }

        function getStreetbox() {
            if ($this->latitude != null && $this->longitude != null) {
                $zoom = 0.05;
                $bbox_lu = $this->longitude - $zoom;
                $bbox_ru = $this->latitude - $zoom;
                $bbox_lo = $this->longitude + $zoom;
                $bbox_ro = $this->latitude + $zoom;
                $streetmap = "https://www.openstreetmap.org/export/embed.html?bbox=$bbox_lu%2C$bbox_ru%2C$bbox_lo%2C$bbox_ro&amp;layer=mapnik&amp;marker=$this->latitude%2C$this->longitude";

                return $streetmap;
            }
            return null;
        }

        
        function getLink() {
            if ($this->link != null) {
                $box = "<a href='" . $this->link . "' class='card-link' target='_blank'>";
				$box .= DataHandler::cutLink($this->link);
				$box .= "</a>";
                return $box;
            }

            return null;                
        }

        /*
		* Creates HTML-text for representation of this galery
        * 
        * @return   string
		*/
        function getShortDetails() {
		}

        /*
		* Creates an gallery from an associative array
        * 
        * @param 	string[]	$array 		The data as array
        * @return   Gallery
		*/
        static function getFromArray($array) {
            return new Gallery(DataHandler::getEntryOrNull('GalleryID', $array), 
                                DataHandler::getEntryOrNull('GalleryName', $array), 
                                DataHandler::getEntryOrNull('GalleryNativeName', $array), 
                                DataHandler::getEntryOrNull('GalleryCity', $array), 
                                DataHandler::getEntryOrNull('GalleryCountry', $array), 
                                DataHandler::getEntryOrNull('Latitude', $array), 
                                DataHandler::getEntryOrNull('Longitude', $array), 
                                DataHandler::getEntryOrNull('GalleryWebSite', $array));
        }

    }

?>