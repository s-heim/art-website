<?php
    require_once "../../processing/data/models/data_handler.php";

    class Review {

        private $id;
        private $artworkID;
        private $artwork;
        private $customerID;
        private $customer;
        private $date;
        private $rating;
        private $comment;

        /*
		* Initializes the subject object
        * 
        * @param 	int 	$id 		The id of the subject
        * @param 	int 	$artworkID  The referencing artwork id
        * @param 	int 	$customerID The referencing customer id
        * @param    string  $date       The date of the review
        * @param    int     $rating     The rating
        * @param    string  $comment    The commentary text
		*/
        function __construct($id, $artworkID, $customerID, $date, $rating, $comment) {
            $this->id = intval($id);
            $this->artworkID = intval($artworkID);
            $this->customerID = intval($customerID);
            $this->date = new DateTime($date);
            $this->rating = intval($rating);
            $this->comment = $comment;
        }

        function getID() {
            return $this->id;
        }

        function getComment() {
            return $this->comment;
        }

        function getDate() {
            return $this->date;
        }

        function getCustomerID() {
            return $this->customerID;
        }

        function getArtworkID() {
            return $this->artworkID;
        }

        function setCustomer($customer) {
            if ($customer->getID() == $this->customerID) {
                $this->customer = $customer;
            }
        }

        function setArtwork($artwork) {
            if ($artwork->getID() == $this->artworkID) {
                $this->artwork = $artwork;
            }
        }

        function show($isAdmin) {
            $box = "<div class='mt-3 mb-0'>";
            $box .= "<span class='font-weight-bold'>";
            $box .= "Review by " . $this->customer->getName();
            $box .= " (" . $this->date->format('jS M Y') . ") ";
            $box .= "from " . $this->customer->getLocation() . ":";
            $box .= "</span>";
            $box .= "<div class='text-justify'>";
            $box .= "<p>";
            $box .= "<span class='font-italic'>";
            $box .= "Rating: " . $this->rating . "/5 stars";
            $box .= "</span>";
            $box .= "<br />";
            $box .= $this->comment;
            $box .= "</p>";
            $box .= "</div>";
            if ($isAdmin) {
                $box .= "<div class='mt-0 mb-3'>";
                $box .= "<form action='../single/artwork.php?id=" . $this->artworkID . "' method='post'>";
			    $box .= "<button type='submit' class='btn btn-secondary btn-sm' name='delreview' value='" . $this->id . "'>";
			    $box .= "Delete Review";
			    $box .= "</button>";
			    $box .= "</form>";
                $box .= "</div>";
            }
            $box .= "</div>";
            return $box;
        }

        /*
		* Creates a subject from an associative array
        * 
        * @param 	string[]	$array 		The data as array
        * @return   Subject
		*/
        static function getFromArray($array) {
            return new Review(DataHandler::getEntryOrNull('ReviewID', $array), 
                                DataHandler::getEntryOrNull('ArtWorkID', $array), 
                                DataHandler::getEntryOrNull('CustomerID', $array), 
                                DataHandler::getEntryOrNull('ReviewDate', $array), 
                                DataHandler::getEntryOrNull('Rating', $array), 
                                DataHandler::getEntryOrNull('Comment', $array));
        }

    }

?>