<?php
    require_once "../../processing/data/models/data_handler.php";

    class Customer {

        private $id;
        private $username;
        private $password;
        private $state;
        private $dateJoined;
        private $dateLastModified;
        private $firstName;
        private $lastName;
        private $address;
        private $city;
        private $region;
        private $country;
        private $postal;
        private $phone;
        private $email;
        private $privacy;
        private $isAdmin;

        /*
		* Initializes the customer object
        * 
        * @param 	int 	$id 		        The id of the customer
        * @param 	string 	$username           The user name
        * @param 	string 	$password           The (hashed) password
        * @param 	int 	$state              The state
        * @param 	string 	$dateJoined         The date joined
        * @param 	string 	$dateLastModified   The date of the last modify
        * @param 	string 	$firstName          The first name
        * @param 	string 	$lastName           The last name
        * @param 	string 	$address            The address
        * @param 	string 	$region             The region
        * @param 	string 	$postal             The postal
        * @param 	string 	$phone              The phone
        * @param 	string 	$email              The email
        * @param 	string 	$privacy            The privacy settings
        * @param 	int 	$isAdmin            Is admin (1) or not (0)
		*/
        function __construct($id, $username, $password, $state, $dateJoined, $dateLastModified, $firstName, $lastName, $address, $city, $region, $country, $postal, $phone, $email, $privacy, $isAdmin) {
            $this->id = intval($id);
            $this->username = $username;
            $this->password = $password;
            $this->state = intval($state);
            $this->dateJoined = $dateJoined;
            $this->dateLastModified = $dateLastModified;
            $this->firstName = $firstName;
            $this->lastName = $lastName;
            $this->address = $address;
            $this->city = $city;
            $this->region = $region;
            $this->country = $country;
            $this->postal = $postal;
            $this->phone = $phone;
            $this->email = $email;
            $this->privacy = $privacy;
            $this->isAdmin = boolval($isAdmin);
        }

        function verify($password) {
            return password_verify($password, $this->password);
        }

        function changePassword($password) {
            $this->password = password_hash($password, PASSWORD_DEFAULT);
        }

        function update($firstName, $lastName, $address, $city, $region, $country, $postal, $phone, $email, $privacy) {
            $this->firstName = $firstName;
            $this->lastName = $lastName;
            $this->address = $address;
            $this->city = $city;
            $this->region = $region;
            $this->country = $country;
            $this->postal = $postal;
            $this->phone = $phone;
            $this->email = $email;
            $this->privacy = $privacy;
        }

        /*
		* Adds the first and last name when both available
        * 
        * @return   string
		*/
        function getName() {
            if (isset($this->firstName)) {
                return $this->firstName . " " . $this->lastName;
			}
            return $this->lastName;
        }

        function getID() {
            return $this->id;
        }

        function getUserName() {
            return $this->username;
        }

        function getFirstName() {
            return $this->firstName;
        }

        function getLastName() {
            return $this->lastName;
        }

        function getEmail() {
            return $this->email;
        }

        function getPhone() {
            return $this->phone;
        }

        function getAddress() {
            return $this->address;
        }

        function getCity() {
            return $this->city;
        }

        function getRegion() {
            return $this->region;
        }

        function getCountry() {
            return $this->country;
        }

        function getPostal() {
            return $this->postal;
        }

        function getPrivacy() {
            return $this->privacy;
        }

        function isAdmin() {
            return $this->isAdmin;
        }

        function getLocation() {
            return $this->city . " (" . $this->country . ")";
        }

        function bindSqlValues($statement, $type) {
            if ($type != "add" && $type != "update" && $type != "password" && $type != "id") {
                return $statement;
            }

            if ($type == "add") {
                $statement->bindValue(":username", $this->username);
                $statement->bindValue(":pass", $this->password);
            }
            else {
                $statement->bindValue(":id", $this->id);
            }

            if ($type == "id") {
                return $statement;
            }
            
            if ($type == "password") {
                $statement->bindValue(":pass", $this->password);
            }
            else {
                $statement->bindValue(":firstname", $this->firstName);
                $statement->bindValue(":lastname", $this->lastName);
                $statement->bindValue(":address", $this->address);
                $statement->bindValue(":city", $this->city);
                $statement->bindValue(":region", $this->region);
                $statement->bindValue(":country", $this->country);
                $statement->bindValue(":postal", $this->postal);
                $statement->bindValue(":phone", $this->phone);
                $statement->bindValue(":email", $this->email);
                $statement->bindValue(":privacy", $this->privacy);
            }
			
            return $statement;
        }

        /*
		* Creates a customer from an associative array
        * 
        * @param 	string[]	$array 		The data as array
        * @return   Customer
		*/
        static function getFromArray($array) {
            return new Customer(DataHandler::getEntryOrNull("CustomerID", $array), 
                                DataHandler::getEntryOrNull("UserName", $array), 
                                DataHandler::getEntryOrNull("Pass", $array), 
                                DataHandler::getEntryOrNull("State", $array), 
                                DataHandler::getEntryOrNull("DateJoined", $array), 
                                DataHandler::getEntryOrNull("DateLastModified", $array), 
                                DataHandler::getEntryOrNull("FirstName", $array), 
                                DataHandler::getEntryOrNull("LastName", $array), 
                                DataHandler::getEntryOrNull("Address", $array), 
                                DataHandler::getEntryOrNull("City", $array), 
                                DataHandler::getEntryOrNull("Region", $array), 
                                DataHandler::getEntryOrNull("Country", $array), 
                                DataHandler::getEntryOrNull("Postal", $array), 
                                DataHandler::getEntryOrNull("Phone", $array), 
                                DataHandler::getEntryOrNull("Email", $array), 
                                DataHandler::getEntryOrNull("Privacy", $array), 
                                DataHandler::getEntryOrNull("IsAdmin", $array));
        }

    }

?>