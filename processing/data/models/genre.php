<?php
    require_once "../../processing/data/models/data_handler.php";

    class Genre {

        private $id;
        private $name;
        private $era;
        private $description;
        private $link;
        private $artworks = array();

        /*
		* Initializes the genre object
        * 
        * @param 	int 	$subjectID 		The id of the genre
        * @param 	string 	$name 	        The name
        * @param 	int 	$era         	The era
        * @param 	string 	$description 	The description
        * @param 	string 	$link        	The link
		*/
        function __construct($id, $name, $era, $description, $link) {
            $this->id = intval($id);
            $this->name = $name;
            $this->era = intval($era);
            $this->description = $description;
            $this->link = $link;
        }

        /*
		* Adds an artwork reference to this genre
        * 
        * @param 	Artwork	$artwork 		The artwork object
		*/
        function addArtwork($artwork) {
            $this->artworks[count($this->artworks)] = $artwork;
        }

        /*
		* Adds an array of artworks to this genre
        * 
        * @param 	Artwork[]	$artworks 		An array of artwork objects
		*/
        function addArtworks($artworks) {
            foreach($artworks as $artwork) {
                $this->addArtwork($artwork);
            }
        }

        function getName() {
            return $this->name;
        }

        function getID() {
            return $this->id;
        }

        function getDescription() {
            return $this->description;
        }

        function getLink() {
            return "<a href='" . $this->link . "' class='card-link' target='_blank'>" . DataHandler::cutLink($this->link) . "</a>";
        }

        function getPicture($size="square-thumbs") {
            return DataHandler::getPicturePath($this->id, "genres", $size);
        }

        /*
		* Creates HTML-text for representation of this genre
        * 
        * @return   string
		*/
        function getShortDetails() {
			$picture = DataHandler::getPicturePath($this->id, "genres", "square-thumbs");
			$box = "<a href='../single/genre.php?id=" . $this->id . "' class='card-link'>";
			$box .= "<button class='btn btn-light btn-block mt-3'>";
			$box .= "<img src='" . $picture . "' alt='Sample art of " . $this->name . "' />";
			$box .= "<br />";
			$box .= $this->name;
			$box .= "</button>";
			$box .= "</a>";
			return $box;
		}

        /*
		* Creates an genre from an associative array
        * 
        * @param 	string[]	$array 		The data as array
        * @return   Genre
		*/
        static function getFromArray($array) {
            return new Genre(DataHandler::getEntryOrNull('GenreID', $array), 
                                DataHandler::getEntryOrNull('GenreName', $array), 
                                DataHandler::getEntryOrNull('Era', $array), 
                                DataHandler::getEntryOrNull('Description', $array), 
                                DataHandler::getEntryOrNull('Link', $array));
        }

    }

?>