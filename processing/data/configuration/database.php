<?php
	require_once "../../processing/data/configuration/config.php";
	
	/*
	* Class to manage the connection to the database
	* @access public
	*/	
	class Database {
		
		/**
		* @var string $conn Saves the connection details using DBHOST and DBNAME from config.php
		*/		
		private $conn = "mysql:host=" . DBHOST . ";dbname=" . DBNAME;
		
		/**
		* @var string $user Saves the user name using DBUSER from config.php
		*/
		private $user = DBUSER;
		
		/**
		* @var string $user Saves the password using DBPASS from config.php
		*/
		private $pass = DBPASS;
		
		/**
		* @var pdo $pdo Saves the usable database connection
		*/
		private $pdo;
		
		
		/**
		* Starts the connection to the database by initializing the pdo object
		*/	
		public function connect() {
			if ($this->pdo != null) {
				return;
			}
			try {
				$this->pdo = new PDO($this->conn, $this->user, $this->pass);
				$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} 
			catch (PDOException $ex) {
				$test = error_log("DB Connection failed: " . $ex->getMessage() . "\n", 3, "../../logs/db_error.log");
				header("Location: ../root/error.php");
				echo $test;
				die();
			}
		}

		/*
		* Runs a sql statement
		* 
		* @param 	string 	$sql 		The SQL string
		* @param 	array	$values		Values to bind
		* @param 	object	$object		Complex Object that bindSqlValues($statement, $type)
		* @return 	data from the database
		*/
		public function runStatement($sql, $values = [], $object = null, $type = null) {
			if ($this->pdo == null) {
				$this->connect();
			}

			$data = null;

			try {
				$statement = $this->pdo->prepare($sql);

				$counter = 1;
				
				foreach ($values as $value) {
					if (is_array($value)) {
						if (isset($value[1])) {
							$statement->bindValue($value[0], $value[1]);
						}
					}
					else {
						$statement->bindValue($counter, $value);
					}
					$counter++;
				}

				if ($object != null && $type != null) {
					$statement = $object->bindSqlValues($statement, $type);
				}

				$statement->execute();
				return $statement->fetchAll();
			} 
			catch (PDOException $ex) {
				$test = error_log("DB Connection failed: " . $ex->getMessage() . "\n", 3, "../../logs/db_error.log");
				echo $ex;
				//header("Location: ../root/error.php");
				die();
			}
		}
		
		/*
		* Interrupts the database connection by setting the pdo object null
		*/		
		public function close() {
			if ($this->pdo == null) {
				return;
			}
			$this->pdo = null;
		}
		
	}
	
?>