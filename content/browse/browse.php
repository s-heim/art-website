<?php 
	if (!isset($_SESSION)) {
		session_start();
	}
	
	require_once "../../processing/data/repositories/artworks_repository.php";
	require_once "../../processing/data/repositories/artists_repository.php";
	require_once "../../processing/data/repositories/genres_repository.php";
	require_once "../../processing/data/repositories/subjects_repository.php";

	function getData($entity) {
		switch ($entity) {
			case ("artists"):
				$artistrepo = new ArtistsRepository();
				$artists = $artistrepo->getAllArtists();
				return $artists;
			case ("artworks"):
				$artworkrepo = new ArtworksRepository();
				$artistrepo = new ArtistsRepository();
				$artworks = $artworkrepo->getAllArtworks();
				foreach($artworks as $artwork) {
					$artwork->setArtist($artistrepo->getArtistByID($artwork->getArtistID()));
				}
				return $artworks;
			case ("genres"):
				$genrerepo = new GenresRepository();
				$genres = $genrerepo->getAllGenres();
				return $genres;
			case ("subjects"):
				$subjectrepo = new SubjectsRepository();
				$subjects = $subjectrepo->getAllSubjects();
				return $subjects;
		}
	}

	$type = null;

	if (isset($_GET["type"]) && !empty($_GET["type"]) && in_array($_GET["type"], array("artists", "artworks", "genres", "subjects"))) {
		$type = $_GET["type"];
	}
	else {
		$websiteTitle = "Browse";
		require_once "../root/header.php";
		echo "<h1>Browse</h1>";
		echo "<p>You cannot browse through this topic!</p>";
		require_once "../root/footer.html"; 
		die();	
	}

	$data = getData($type); 
	
	$websiteTitle = "Browse " . ucfirst($type);
	require_once "../root/header.php";
?>
	
<h1>Browse <?php echo ucfirst($type); ?></h1>
<p>Here you can browse throw all our <?php echo $type; ?>. Just click on the one, you want to learn more about.</p>

<div class="row">
	<?php 
		
		foreach($data as $entry) {
			echo "<div class='col-2 mt-3'>" . $entry->getShortDetails("artist") . "</div>";
		}
	?>
</div>

<?php require_once "../root/footer.html"; ?>