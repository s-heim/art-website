<?php 
	$login = "Login";
	$name = null;
	if (isset($_SESSION["UserID"])) {
		require_once "../../processing/helper/prepare_functions.php"; 
		$user = PrepareFunctions::getSingleUserDetails($_SESSION["UserID"]);
		$name = $user->getName();
		$login = "Logout";
	}

		
	function sanitize_output($buffer) {
		$search = [
			'/\>[^\S ]+/s',
			'/[^\S ]+\</s',
			'/(\s)+/s',
			'/<!--(.|\s)*?-->/'
		];
		
		$replace = [
			'>',
			'<',
			'\\1',
			''
		];
		
		$buffer = preg_replace($search, $replace, $buffer);
		
		return $buffer;
	}

	ob_start("sanitize_output");
?>

<!DOCTYPE html>

<html lang="en">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<link rel="preconnect" href="https://fonts.gstatic.com" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Indie+Flower&family=Raleway&display=swap" /> 
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous" />
		<link rel="stylesheet" type="text/css" href="../root/style.css">
		<link rel="icon" type="image/x-icon" href="../../img/layout/logo-icon.ico">
		
		<title><?php echo $websiteTitle; ?></title>
	</head>
	
	
	<body>
	
		<div class="container-fluid mb-3 mt-3">

			<nav class="navbar navbar-expand-lg navbar-light bg-light mb-3 mt-3">
				<div class="collapse navbar-collapse">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a class="nav-link" href="../root/index.php">
								<img src="../../img/layout/logo-small.png" alt="logo" />
							</a>
						</li>
						<li class="nav-item active">
							<a class="nav-link" href="../root/index.php">
								Start
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="../about/about_us.php">About Us</a>
						</li>
						<li class="nav-item dropdown align-middle">
							<a class="nav-link dropdown-toggle" role="button" data-toggle="dropdown">Browse</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../browse/browse.php?type=artists">Browse Artists</a>
								<a class="dropdown-item" href="../browse/browse.php?type=artworks">Browse Artworks</a>
								<a class="dropdown-item" href="../browse/browse.php?type=genres">Browse Genres</a>
								<a class="dropdown-item" href="../browse/browse.php?type=subjects">Browse Subjects</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" role="button" data-toggle="dropdown">Search</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../search/search_results.php">Simple Search</a>
								<a class="dropdown-item" href="../search/advanced_search.php">Advanced Search</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" role="button" data-toggle="dropdown">Utilities</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<?php 
									if (!isset($_SESSION["UserID"])) {
										echo "<a class='dropdown-item' href='../utilities/register.php'>Register</a>";
									}
								?>
								<a class="dropdown-item" href="../utilities/my_account.php">My Account</a>
								<a class="dropdown-item" href="../utilities/favorite_list.php">Favorite List</a>
								<?php
									if (isset($_SESSION["IsAdmin"]) && $_SESSION["IsAdmin"]) {
										echo "<a class='dropdown-item' href='../utilities/manage_users.php'>Manage Users</a>";
									}
								?>
							</div>
						</li>
						<li class="nav-item">
							<form action="../utilities/<?php echo strtolower($login); ?>.php" method="post">
								<button class="btn btn-outline-dark">
									<?php 
										echo $login; 
										if (isset($name)) {
											echo ": ";
											echo $name;
										}
									?>
								</button>
							</form>
						</li>
					</ul>
					
					<form class="form-inline my-2 my-lg-0" action="../search/search_results.php" method="get">
						<input type="text" class="form-control mr-sm-2" placeholder="Search" name="search" />
						<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
					</form>
				</div>
			</nav>