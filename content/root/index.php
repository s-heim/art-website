<?php 
	if (!isset($_SESSION)) {
		session_start();
	}
	$websiteTitle = "Start";
	require_once "../root/header.php"; 
	
	require_once "../../processing/data/repositories/artworks_repository.php";
	require_once "../../processing/data/repositories/artists_repository.php";
	require_once "../../processing/data/repositories/customers_repository.php";
	require_once "../../processing/data/repositories/reviews_repository.php";

	$artworkrepo = new ArtworksRepository();
	$artistrepo = new ArtistsRepository();
	$customerrepo = new CustomersRepository();
	$reviewrepo = new ReviewsRepository();

	$topArtworks = $artworkrepo->getTopArtworks();
	$topArtworksBox = "";
	foreach ($topArtworks as $top) {
		$artwork = $artworkrepo->getArtWorkByID($top["ArtWorkID"]);
		$artwork->setArtist($artistrepo->getArtistByID($artwork->getArtistID()));
		$rating = $artworkrepo->getArtworkRatingByID($artwork->getID());
		$topArtworksBox .= "<div class='mt-3'>";
		$topArtworksBox .= $artwork->getShortDetails("artist", $rating[0]["Ratings"]);
		$topArtworksBox .= "</div>";
	}

	$topArtists = $artistrepo->getTopArtists();
	$topArtistsBox = "";
	foreach ($topArtists as $top) {
		$topArtistsBox .= "<div class='mt-3'>";
		$topArtistsBox .= $artistrepo->getArtistByID($top["ArtistID"])->getShortDetails($top["AvgRating"]);
		$topArtistsBox .= "</div>";
	}

	$newArtworks = $artworkrepo->getNewestArtworks();
	$newArtworksBox = "";
	foreach ($newArtworks as $new) {
		$artwork = $artworkrepo->getArtWorkByID($new["ArtWorkID"]);
		$artwork->setArtist($artistrepo->getArtistByID($artwork->getArtistID()));
		$newArtworksBox .= "<div class='mt-3'>";
		$newArtworksBox .= $artwork->getShortDetails("artist");
		$newArtworksBox .= "</div>";
	}

	$newReviews = $reviewrepo->getRecentReviews();
	$newReviewsBox = "";

	foreach ($newReviews as $new) {
		$artwork = $artworkrepo->getArtWorkByID($new->getArtworkID());
		$artwork->setArtist($artistrepo->getArtistByID($artwork->getArtistID()));
		$reviewer = $customerrepo->getCustomerByID($new->getCustomerID());
		$newReviewsBox .= "<p class='mt-3 mb-3'>";
		$newReviewsBox .= $artwork->getShortDetails("artist");
		$newReviewsBox .= "</p>";
		$newReviewsBox .= "<p class='text-left'>";
		$newReviewsBox .= "Review by " . $reviewer->getName();
		$newReviewsBox .= " (" . $new->getDate()->format('jS M Y') . "): <br />";
		$newReviewsBox .= substr($new->getComment(), 0, 100) . "...";
		$newReviewsBox .= "</p>";
	}
	
?>

<div id="Carousel" class="carousel slide mt-3 mb-3" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#Carousel" data-slide-to="0" class="active"></li>
		<li data-target="#Carousel" data-slide-to="1"></li>
		<li data-target="#Carousel" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner">
		<div class="carousel-item active">
			<div class="d-block bg-secondary text-white text-center p-4">
				<h3>Browsing Art</h3>
				<p>We have four categories to browse: artists, artworks, artwork genres and artwork subjects.</p>
				<p>There are over 300 artworks from over 80 artists in our database to discover.</p>
			</div>
		</div>
		<div class="carousel-item">
			<div class="d-block bg-secondary text-white text-center p-4">
				<h3>Searching Art</h3>
				<p>If you search for a specific artist or artwork, use the seach function.</p>
				<p>Just type in the last name of the artist or the artwork's title in the search box.</p>
			</div>
		</div>
		<div class="carousel-item">
			<div class="d-block bg-secondary text-white text-center p-4">
				<h3>Rating Art</h3>
				<p>Leave your review with a personal rating to each artwork in our database.</p>
				<p>The Top 3 rated artwork is showed on the first side.</p>
			</div>
		</div>
	</div>
	<a class="carousel-control-prev" href="#Carousel" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#Carousel" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>

<h1>Welcome</h1>
<p>Browse our artwork database, add reviews to the most recent artworks and vote for your favorite artist of time.</p>

<div class="row">
	<div class="col">
		<h2>Top 3 Artworks</h2>
		<div class="mt-3 mb-3">
			<?php echo $topArtworksBox; ?>
		</div>
	</div>
	<div class="col">
		<h2>Top 3 Artists</h2>
		<div class="mt-3 mb-3">
			<?php echo $topArtistsBox; ?>
		</div>
	</div>
	<div class="col">
		<h2>New Additions</h2>
		<div class="mt-3 mb-3">
			<?php echo $newArtworksBox; ?>
		</div>
	</div>
	<div class="col">
		<h3>Recent Reviews</h3>
		<div class="mt-3 mb-3">
			<?php echo $newReviewsBox; ?>
		</div>
	</div>
</div>

<?php require_once "../root/footer.html"; ?>