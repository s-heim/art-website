<?php
	$websiteTitle = "Error";
	require_once "../root/header.php"; 
	
?>

<h1>Error</h1>

<p>An error occured while connecting to the database, please try later again!</p>
<p class="font-italic"><a href="../root/" class="card-link">Back to Start Page</a></p>

<?php require_once "../root/footer.html"; ?>