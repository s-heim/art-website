const deleteUserElements = document.getElementsByName("deleteUser");
if (deleteUserElements) {
    deleteUserElements.forEach((element) => {
        element.onclick = function() {
            return confirm("Are sure to deactivate this user? All data related to reviews and ratings will survive.");
        }
    })
}


const makeAdminElements = document.getElementsByName("makeAdmin");
if (makeAdminElements) {
    makeAdminElements.forEach((element) => {
        element.onclick = function() {
            return confirm("Are sure to make this user an administrator?");
        }
    })
}

const deleteReviewElements = document.getElementsByName("delreview");
if (deleteReviewElements) {
    deleteReviewElements.forEach((element) => {
        element.onclick = function() {
            return confirm("Are sure to delete this review?");
        }
    })
}

const deleteFavArtworks = document.getElementsByName("delfavartwork");
if (deleteFavArtworks) {
    deleteFavArtworks.forEach((element) => {
        element.onclick = function() {
            return confirm("Are sure remove this artwork from your favorite list?");
        }
    })
}

const deleteFavArtists = document.getElementsByName("delfavartist");
if (deleteFavArtists) {
    deleteFavArtists.forEach((element) => {
        element.onclick = function() {
            return confirm("Are sure remove this artist from your favorite list?");
        }
    })
}

const updateCustomerElements = document.getElementsByName("update-customer");
if (updateCustomerElements) {
    updateCustomerElements.forEach((element) => {
        element.onclick = function() {
            return confirm("Are you sure that the changes are correct?");
        }
    })
}

const updateAccountElements = document.getElementsByName("update-account");
if (updateAccountElements) {
    updateAccountElements.forEach((element) => {
        element.onclick = function() {
            return confirm("Are you sure that the changes are correct?");
        }
    })
}

const updatePasswordElements = document.getElementsByName("update-password");
if (updatePasswordElements) {
    updatePasswordElements.forEach((element) => {
        element.onclick = function() {
            return confirm("Are you sure to change your password?");
        }
    })
}