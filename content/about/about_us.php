<?php 
	if (!isset($_SESSION)) {
		session_start();
	}
	
	$websiteTitle = "About Us";
	require_once "../root/header.php"; 
?>

<h1>About Us</h1>

<p>This site was created for a term project in the study module Dynamic Internetworking by students of the Technical University of Applied Sciences Wildau . It is hypothetical and the authors give no warranty of full correctness and safety.</p>

<h2>The group members:</h2>

<div class="row">
	<div class="col m-3 text-center">
		<div class="m-3"><img src="../../img/works/square-medium/133020.jpg" alt="art portrait" /></div>
		<ul class="list-unstyled">
			<li>Layout & Arrangement</li>
			<li>Form Handling</li>
		</ul>
	</div>
	<div class="col m-3 text-center">
		<div class="m-3"><img src="../../img/works/square-medium/097030.jpg" alt="art portrait" /></div>
		<ul class="list-unstyled">
			<li>Database Access & Formatting</li>
			<li>Sort Functions</li>
		</ul>
	</div>
	<div class="col m-3 text-center">
		<div class="m-3"><img src="../../img/works/square-medium/096030.jpg" alt="art portrait" /></div>
		<ul class="list-unstyled">
			<li>User Handling</li>
			<li>Testing</li>
		</ul>
	</div>
	<div class="col m-3 text-center">
		<div class="m-3"><img src="../../img/works/square-medium/100010.jpg" alt="art portrait" /></div>
		<ul class="list-unstyled">
			<li>Detail Pages</li>
			<li>Search Functions</li>
		</ul>
	</div>
</div>


<?php
	require_once "../root/footer.html"; 
?>