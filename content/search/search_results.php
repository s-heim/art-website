<?php 
	if (!isset($_SESSION)) {
		session_start();
	}

	require_once "../../processing/helper/search_functions.php"; 
	
	$sortartworksdir = null;
	$sortartistsdir = null;
	$sortartists = null;
	$sortartworks = null;
	$search = null;
	$artists = null;
	$artworks = null;
	if (isset($_GET["sortartworksdir"])) {
		$sortartworksdir = SearchFunctions::validateParamInput($_GET["sortartworksdir"], "asc", array("asc", "desc"));
	}
	if (isset($_GET["sortartistsdir"])) {
		$sortartistsdir = SearchFunctions::validateParamInput($_GET["sortartistsdir"], "asc", array("asc", "desc"));
	}
	if (isset($_GET["sortartists"])) {
		$sortartists = SearchFunctions::validateParamInput($_GET["sortartists"], "artistsid", array("last name"));
	}
	if (isset($_GET["sortartworks"])) {
		$sortartworks = SearchFunctions::validateParamInput($_GET["sortartworks"], "artworkid", array("title", "msrp", "year"));
	}
	if (isset($_GET["search"])) {
		$search = $_GET["search"];
		$artists = SearchFunctions::validateSearchInput("artists", $_GET["search"], $sortartistsdir, $sortartists);
		$artworks = SearchFunctions::validateSearchInput("artworks", $_GET["search"], $sortartworksdir, $sortartworks);
	}

	$websiteTitle = "Search Results";
	require_once "../root/header.php"; 
?>

<h1>Search Results</h1>

<p>Type in an artist's <i>last name</i> or artwork's <i>title</i> to search in the database!</p>

<form class="form-inline mb-3" action="../Search/search_results.php" method="get">
	<input type="text" class="form-control" placeholder = "Search by pressing enter" name="search" value = "<?php echo $search; ?>" />
</form>

<?php 
	if (empty($search)) {
		require_once "../root/footer.html";
		die();
	}
?>

<div class="row">
	<?php if ($artists != null) { ?>
		<div class="col">
			<h2>Matching artists:</h2>
			<div class="mt-3 mb-3">
				<form action="../Search/search_results.php" method="get">
					<input type="hidden" name="search" value="<?php echo $search; ?>" />
					<input type="hidden" name="sortartworksdir" value="<?php echo $sortartworksdir; ?>" />
					<input type="hidden" name="sortartworks" value="<?php echo $sortartworks; ?>" />
					<?php echo SearchFunctions::getSortBox("artists", array("last name"), $sortartistsdir); ?>
				</form>
			</div>
			<div class="row">
				<?php 
					$size = "lg";
					if ($artworks != null) {
						$size = "sm";
					}
					echo SearchFunctions::showMatchingArtists($artists, $size); 
				?>
			</div>
		</div>
	<?php } ?>
	
	<?php if ($artworks != null) { ?>
		<div class="col">
			<h2>Matching artworks:</h2>
			<div class="mt-3 mb-3">
				<form action="../Search/search_results.php" method="get">
					<input type="hidden" name="search" value="<?php echo $search; ?>" />
					<input type="hidden" name="sortartistsdir" value="<?php echo $sortartistsdir; ?>" />
					<input type="hidden" name="sortartists" value="<?php echo $sortartists; ?>" />
					<?php echo SearchFunctions::getSortBox("artworks", array("title", "msrp", "year"), $sortartworksdir); ?>
				</form>
			</div>
			<div class="row">
				<?php 
					$size = "lg";
					if ($artists != null) {
						$size = "sm";
					}
					echo SearchFunctions::showMatchingArtworks($artworks, $size); 
				?>
			</div>
		</div>
	<?php } ?>
</div>

<?php require_once "../root/footer.html"; ?>