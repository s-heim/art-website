<?php 
	if (!isset($_SESSION)) {
		session_start();
	}

	require_once "../../processing/helper/search_functions.php"; 
	require_once "../../processing/helper/prepare_functions.php"; 
	
	$result = null;
	$first = null;
	$firstRange = null; 
	$firstButtonArtworks = "btn btn-primary";
	$firstButtonArtists= "btn btn-primary";
	$second = null;
	$secondKeywordLabel = null;
	$secondKeywordLabel2 = null;
	$secondKeyword = null;
	$secondKeyword2 = null;
	$secondRangeLabel = null;
	$secondRangeMin = null;
	$secondRangeMax = null;
	$secondSelectLabel = null;
	$secondSelectList = null;
	$secondSelectLabel2 = null;
	$secondSelectList2 = null;
	$secondSelection = null;
	$secondSelection2 = null;
	
	$sortdir = null;
	$sortparam = null;
	
	if (isset($_GET["sortartworksdir"])) {
		$sortdir = SearchFunctions::validateParamInput($_GET["sortartworksdir"], "asc", array("asc", "desc"));
	}
	if (isset($_GET["sortartistsdir"])) {
		$sortdir = SearchFunctions::validateParamInput($_GET["sortartistsdir"], "asc", array("asc", "desc"));
	}
	if (isset($_GET["sortartists"])) {
		$sortparam = SearchFunctions::validateParamInput($_GET["sortartists"], "artistsid", array("last name"));
	}
	if (isset($_GET["sortartworks"])) {
		$sortparam = SearchFunctions::validateParamInput($_GET["sortartworks"], "artworkid", array("title", "msrp", "year"));
	}
	
	if (isset($_GET["first"])) {
		switch ($_GET["first"]) {
			case "artworks": 
				$first = "artworks";
				$firstRange = array("title", "msrp", "year");
				$firstButtonArtworks = "btn btn-secondary";
				$secondKeywordLabel = "Artwork Title";
				$secondRangeLabel = "Year of Work";
				$secondSelectLabel = "Genre";
				$secondSelectList = PrepareFunctions::getAllGenresForSearch();
				$secondSelectLabel2 = "Subject";
				$secondSelectList2 = PrepareFunctions::getAllSubjectsForSearch();	
				break;
			case "artists": 
				$first = "artists";
				$firstRange = array("last name");
				$firstButtonArtists = "btn btn-secondary";
				$secondKeywordLabel = "Last Name";
				$secondKeywordLabel2 = "First Name";
				$secondRangeLabel = "Living Range";
				$secondSelectLabel = "Nationality";
				$secondSelectList = PrepareFunctions::getAllNationalitiesForSearch();	
				break;	
		}
	}

	if (isset($_GET["second"])) {
		$second = true;
		if (isset($_GET["keyword"]) && (!empty($_GET["keyword"]))) {
			$secondKeyword = trim($_GET["keyword"]);
		}
		if (isset($_GET["keyword2"]) && (!empty($_GET["keyword2"]))) {
			$secondKeyword2 = trim($_GET["keyword2"]);
		}
		if (isset($_GET["min"]) && (is_numeric($_GET["min"]))) {
			$secondRangeMin = $_GET["min"];
		}
		if (isset($_GET["max"]) && (is_numeric($_GET["max"]))) {
			$secondRangeMax = $_GET["max"];
		}
		if (isset($_GET["select"]) && (!empty($_GET["select"]))) {
			$secondSelection = trim($_GET["select"]);
		}
		if (isset($_GET["select2"]) && (!empty($_GET["select2"]))) {
			$secondSelection2 = trim($_GET["select2"]);
		}
		$result = SearchFunctions::validateAdvancedSearchInput($first, $secondKeyword, null, $secondRangeMin, $secondRangeMax, $secondSelection, $secondSelection2, $sortdir, $sortparam);
	}

	$websiteTitle = "Advanced Search";
	require_once "../root/header.php"; 
?>

<h1>Advanced Search</h1>

<p>Here you can search for artworks or artists with advances accuracy.</p>

<div class="mt-3 mb-3">
	<h2>First: What are you looking for?</h2>
	<form action="advanced_search.php" method="get">
		<div class="m-3 d-inline">
			<button class="<?php echo $firstButtonArtworks; ?>" type="submit" name="first" value="artworks">Artworks</button>
		</div>
		<div class="m-3 d-inline">
			<button class="<?php echo $firstButtonArtists; ?>" type="submit" name="first" value="artists">Artists</button>
		</div>
	</form>
</div>

<?php 
	if ($first == null) {
		require_once "../root/footer.html"; 
		die();
	}
?>	

<div class="mt-3 mb-3">
	<h2>Second: Adjust the parameters</h2>
	<form action="advanced_search.php" method="get">
		<input type="hidden" name="first" value="<?php echo $first; ?>" />
		<input type="hidden" name="second" value=true />
		<div class="row form-group">
			<div class="col">
				<label for="keyword"><?php echo $secondKeywordLabel; ?></label>
				<input type="text" class="form-control" id="keyword" name="keyword" value="<?php echo $secondKeyword; ?>" />
			</div>
			<?php if (isset($secondKeywordLabel2)) { ?>
			<div class="col">
				<label for="keyword2"><?php echo $secondKeywordLabel2; ?></label>
				<input type="text" class="form-control" id="keyword2" name="keyword2" value="<?php echo $secondKeyword2; ?>" />
			</div>
			<?php } ?>
		</div>
		<div class="row form-group">
			<div class="col">
				<label for="min"><?php echo $secondRangeLabel; ?> - Minimum</label>
				<input type="number" class="form-control" id="min" name="min" min="1200" max="2000" step="1" value="<?php echo $secondRangeMin; ?>" />
			</div>
			<div class="col">
				<label for="max"><?php echo $secondRangeLabel; ?> - Maximum</label>
				<input type="number" class="form-control" id="max" name="max" min="1200" max="2000" step="1" value="<?php echo $secondRangeMax; ?>" />
			</div>
		</div>
		<div class="row form-group">
			<div class="col">
				<label for="select"><?php echo $secondSelectLabel; ?></label>
				<select class="form-control" id="select" name="select">
					<option></option>
					<?php
						foreach ($secondSelectList as $listItem) {
							if ($secondSelection == $listItem) {
								echo "<option selected>" . $listItem . "</option>";
							}
							else {
								echo "<option>" . $listItem . "</option>";
							}
						}
					?>
				</select>
			</div>
			<?php if (isset($secondSelectLabel2)) { ?>
			<div class="col">
				<label for="select2"><?php echo $secondSelectLabel2; ?></label>
				<select class="form-control" id="select2" name="select2">
					<option></option>
					<?php
						foreach ($secondSelectList2 as $listItem) {
							if ($secondSelection2 == $listItem) {
								echo "<option selected>" . $listItem . "</option>";
							}
							else {
								echo "<option>" . $listItem . "</option>";
							}
						}
					?>
				</select>
			</div>
			<?php } ?>
		</div>
		
		<button type="submit" class="btn btn-primary">Search</button>
	</form>
</div>

<?php 
	if ($second == null) {
		require_once "../root/footer.html"; 
		die();
	}
?>	

<div class="row">
	<div class="col">
		<h2>Matching <?php echo $first; ?>:</h2>
		<div class="mt-3 mb-3">
			<form action="../Search/advanced_search.php" method="get">
				<input type="hidden" name="first" value="<?php echo $first; ?>" />
				<input type="hidden" name="second" value=true />
				<input type="hidden" name="keyword" value="<?php echo $secondKeyword; ?>" />
				<input type="hidden" name="keyword2" value="<?php echo $secondKeyword2; ?>" />
				<input type="hidden" name="min" value="<?php echo $secondRangeMin; ?>" />
				<input type="hidden" name="max" value="<?php echo $secondRangeMax; ?>" />
				<input type="hidden" name="select" value="<?php echo $secondSelection; ?>" />
				<?php echo SearchFunctions::getSortBox($first, $firstRange, $sortdir); ?>
			</form>
		</div>
		<div class="row">
			<?php
				if ($first == "artists") {
					echo SearchFunctions::showMatchingArtists($result, "lg");
				}
				else if ($first == "artworks") {
					echo SearchFunctions::showMatchingArtworks($result, "lg");
				}
			?>
		</div>
	</div>
</div>

<?php require_once "../root/footer.html"; ?>