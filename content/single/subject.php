<?php 
	if (!isset($_SESSION)) {
		session_start();
	}
	require_once "../../processing/helper/search_functions.php";
	require_once "../../processing/helper/prepare_functions.php";
	
	$id = 0;
	if (isset($_GET["id"])) {
		$id = PrepareFunctions::validateIdInput($_GET["id"]);
	}

	$subjectrepo = new SubjectsRepository();
	$subject = $subjectrepo->getSubjectByID($id);

	if ($subject->getID() == null) {
		$websiteTitle = "Unknown Subject";
		require_once "../root/header.php";
		echo "<h1>Unknown Subject</h1>";
		echo "<p>This Subject does not exist!</p>";
		require_once "../root/footer.html"; 
		die();
	}
	else {
		$websiteTitle = $subject->getName();
	}
				
	$artworkrepo = new ArtworksRepository();
	$artworks = $artworkrepo->getArtWorksForSubject($subject->getID());
	$subject->addArtworks($artworks);

	$sortartworksdir = null;
	$sortartworks = null;
	if (isset($_GET["sortartworksdir"])) {
		$sortartworksdir = SearchFunctions::validateParamInput($_GET["sortartworksdir"], "asc", array("asc", "desc"));
	}
	if (isset($_GET["sortartworks"])) {
		$sortartworks = SearchFunctions::validateParamInput($_GET["sortartworks"], "artworkid", array("title", "msrp", "year"));
	}
	$artworks = SearchFunctions::validateAdvancedSearchInput("artworks", null, null, null, null, null, $subject->getName(), $sortartworksdir, $sortartworks);

	require_once "../root/header.php";
?>

<h1><?php echo $websiteTitle ?></h1>
<div class="row">	
	<div class="col mt-3 mb-3">
		<img src="<?php echo $subject->getPicture();  ?>" alt="Sample art of <?php echo $websiteTitle ?>" />
	</div>
</div>

<div class="row">
	<div class="col">
		<div class="d-block">
			<h2>Art of this subject</h2>
			<div class="mt-3 mb-3">
				<form action="../single/subject.php" method="get">
					<input type="hidden" name="id" value="<?php echo $id; ?>" />
					<?php echo SearchFunctions::getSortBox("artworks", array("title", "msrp", "year"), $sortartworksdir); ?>
				</form>
			</div>
			<div class="row">
				<?php echo SearchFunctions::showMatchingArtworks($artworks, "lg"); ?>
			</div>
		</div>
	</div>
</div>

<?php require_once "../root/footer.html"; ?>