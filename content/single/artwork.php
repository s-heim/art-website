<?php 
	if (!isset($_SESSION)) {
		session_start();
	}
	
	require_once "../../processing/helper/prepare_functions.php";
	require_once "../../processing/helper/show_functions.php";
	require_once "../../processing/helper/user_functions.php";

	require_once "../../processing/data/repositories/artworks_repository.php";
	require_once "../../processing/data/repositories/artists_repository.php";
	require_once "../../processing/data/repositories/genres_repository.php";
	require_once "../../processing/data/repositories/subjects_repository.php";
	require_once "../../processing/data/repositories/galleries_repository.php";
	
	$id = 0;
	if (isset($_GET["id"])) {
		$id = PrepareFunctions::validateIdInput($_GET["id"]);
	}
	if (isset($_SESSION["IsAdmin"]) && $_SESSION["IsAdmin"]) {
		if (isset($_POST["delreview"])) {
			UserFunctions::deleteReviewFromArtwork($_POST["delreview"]);
		}
	}
	
	$artworkrepo = new ArtworksRepository();
	$artwork = $artworkrepo->getArtWorkByID($id);

	if ($artwork->getID() == null) {
		$websiteTitle = "Unknown Artwork";
		require_once "../root/header.php";
		echo "<h1>Unknown Artwork</h1>";
		echo "<p>This Artwork does not exist!</p>";
		require_once "../root/footer.html"; 
		die();
	}
	else {
		$websiteTitle = $artwork->getTitle();
	}

	$artistrepo = new ArtistsRepository();
	$genrerepo = new GenresRepository();
	$subjectrepo = new SubjectsRepository();
	$galleryrepo = new GalleriesRepository();

	$artwork->setArtist($artistrepo->getArtistByID($artwork->getArtistID()));
	$artwork->addGenres($genrerepo->getGenresByArtWork($artwork->getID()));
	$artwork->addSubjects($subjectrepo->getSubjectsByArtWork($artwork->getID()));
	$artwork->setGallery($galleryrepo->getGallerieByID($artwork->getGalleryID()));

	$ratings = $artworkrepo->getArtworkRatingByID($artwork->getID());

	$artwork->setRating($ratings[0]["Ratings"], $ratings[0]["NumberOfReviews"]);

	$favIsset = false;
	$reviewPossible = null;
	if (isset($_SESSION["UserID"])) {
		foreach($_SESSION["FavArtworks"] as $fav) {
			if ($fav == $id) {
				$favIsset = true;
			}
		}
		if (UserFunctions::checkForReview($id, $_SESSION["UserID"])) {
			$reviewPossible = false;
		}
		else {
			if (isset($_POST["comment"])) {
				UserFunctions::addReviewForArtwork($_POST["artwork"], $_POST["user"], $_POST["rating"], $_POST["comment"]);
				$reviewPossible = false;
			}
			else {
				$reviewPossible = true;
			}	
		}
	}
	$streetmap = $artwork->getGallery()->getStreetbox();

	require_once "../root/header.php";
	$title = str_replace("'", "", $artwork->getTitle()) . " (" . $artwork->getYear() . ") by " . str_replace("'", "", $artwork->getArtist()->getName());
?>

<h1><?php echo $websiteTitle ?></h1>
<h2>by 
	<a href="artist.php?id=<?php echo $artwork->getArtistID(); ?>" class="card-link">
		<?php echo $artwork->getArtist()->getName(); ?>
	</a>
</h2>

<div class="row">	
	<div class="col mt-3 mb-3">
		<div class="float-left d-inline mr-3 mb-3">
			<a href="" data-toggle="modal" data-target="#LargePicture">
				<img src='<?php echo $artwork->getPicture();  ?>' alt='Artwork Image: <?php echo $title ?>' />
			</a>
			<div class="modal fade" id="LargePicture" tabindex="-1" role="dialog" aria-labelledby="<?php echo $title; ?>" aria-hidden="true">
			  <div class="modal-dialog modal-lg" role="image">
				<div class="modal-content">
				  <div class="modal-header">
					<h3 class="modal-title" id="exampleModalLabel"><?php echo $title; ?></h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				  </div>
				  <div class="modal-body text-center">
						<img src='<?php echo $artwork->getPicture("large");  ?>' alt='Artwork Image: <?php echo $title ?>' />
				  </div>
				</div>
			  </div>
			</div>
		</div>
		
		<p class="text-justify ml-3">
			<?php echo $artwork->getDescription(); ?>
		</p>
		
		<p class="font-weight-bold text-danger"><?php echo $artwork->getPrice(); ?></p>
		
		<div class="mt-3 mb-3">
			<?php 
				if (isset($_SESSION["UserID"])) {
					echo ShowFunctions::getFavoriteButton("FavArtworks", $id, $favIsset);
				}
			?>
		</div>
		
		<table class="table table-hover d-inline">
			<thead>
				<tr>
					<th scope="col" colspan="2"><h4 class="text-center">Artwork Details</h4></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th scope="row">Date:</th>
					<td><?php echo $artwork->getYear(); ?></td>
				</tr>
				<tr>
					<th scope="row">Medium:</th>
					<td><?php echo $artwork->getMedium() ?></td>
				</tr>
				<tr>
					<th scope="row">Dimensions:</th>
					<td><?php echo $artwork->getDimensions() ?></td>
				</tr>
				<tr>
					<th scope="row">Genres:</th>
					<td><?php echo $artwork->getGenres(); ?></td>
				</tr>
				<tr>
					<th scope="row">Subjects:</th>
					<td><?php echo $artwork->getSubjects(); ?></td>
				</tr>
				<tr>
					<th scope="row">More Info:</th>
					<td>
						<?php echo $artwork->getLink(); ?>
					</td>
				</tr>
				<tr>
					<td scope="col" colspan="2">
						<div id="GalleryAccordion">
							<div class="card">
								<div class="card-header">
									<button class="btn btn-block btn-lg card-link" data-toggle="collapse" data-target="#GalleryCollapse">
										<h4>Home Gallery</h4>
									</button>
								</div>

								<div id="GalleryCollapse" class="collapse show" data-parent="#GalleryAccordion">
									<div class="card-body">
										<table class="table table-hover">
											<?php echo $artwork->getGalleryDetails(); ?>
										</table>
										<?php
											if ($streetmap !=null) {
												echo "<div class='embed-responsive embed-responsive-16by9'>";
												echo "<iframe class='embed-responsive-item' src='" . $streetmap . "'></iframe>";
												echo "</div>";
											}
										?>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
								
<h3>Reviews to this artwork:</h3>
<p class="font-weight-bold">
	Average Rating: <?php echo $artwork->getRating() . " of 5.00 stars (" . $artwork->getVotes() . " votes)"; ?>
</p>
<div class="row">
	<div class="col">
		<?php 
			$reviewrepo = new ReviewsRepository();
			$customerrepo = new CustomersRepository();
			$reviews = $reviewrepo->getReviewsForArtwork($id);		

			if (count($reviews) == 0) {
				echo "<p class='font-italic mb-3'>There is no review yet!</p>";
			}
			if (isset($_SESSION["UserID"])) {
				if ($reviewPossible) {
					echo "<div class='row'>";
					echo "<div class='col-5 mt-3 mb-3'>";
					echo "<h5>Leave your review:</h5>";
					echo "<form action='artwork.php?id=" . $id . "' method='post'>";
					echo "<input type='hidden' name='artwork' value='" . $id . "' />";
					echo "<input type='hidden' name='user' value='" . $_SESSION["UserID"] . "' />";
					echo "<div class='form-group'>";
					echo "<small class='form-check-label' for='rating'>Rating:</small>";
					echo "<input type='range' id='rating' class='custom-range' min='1' max='5' step='1' name='rating' required />";
					echo "<div class='row'>";
					echo "<div class='col text-left'>1</div>";
					echo "<div class='col text-center'>3</div>";
					echo "<div class='col text-right'>5</div>";
					echo "</div>";
					echo "</div>";
					echo "<div class='form-group'>";
					echo "<small class='form-label' for='comment'>Your Comment:</small>";
					echo "<textarea class='form-control' id='comment' name='comment' required></textarea>";
					echo "<small class='form-text text-muted'>You can use &#060;br&#062; for a line break, other HTML-Tags are not allowed!</small>";
					echo "</div>";
					echo "<button type='submit' class='btn btn-primary'>";
					echo "Add Review";
					echo "</button>";
					echo "</form>";
					echo "</div>";
					echo "</div>";
				}
				else {
					echo "<p class='text-danger'>You have already reviewed this artwork!</p>";
				}
			}
			else {
				echo "<p class='text-danger'>You must be logged in to review this artwork. <a href='../utilities/login.php'>Login here!</a>";
			}	
			foreach ($reviews as $review) {
				$reviewer = $customerrepo->getCustomerByID($review->getCustomerID());
				$review->setCustomer($reviewer);
				echo $review->show(isset($_SESSION["IsAdmin"]) && $_SESSION["IsAdmin"]);
			}	
		 ?>
	</div>
</div>

<?php require_once "../root/footer.html"; ?>