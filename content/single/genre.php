<?php 	
	if (!isset($_SESSION)) {
		session_start();
	}
	require_once "../../processing/helper/prepare_functions.php";
	require_once "../../processing/helper/search_functions.php";
	require_once "../../processing/data/repositories/genres_repository.php";

	$id = 0;
	if (isset($_GET["id"])) {
		$id = PrepareFunctions::validateIdInput($_GET["id"]);
	}

	$genrerepo = new GenresRepository();
	$genre = $genrerepo->getGenreByID($id);

	$artworkrepo = new ArtworksRepository();
	$artworks = $artworkrepo->getArtWorksForGenre($genre->getID());
	$genre->addArtworks($artworks);

	if ($genre->getID() == null) {
		$websiteTitle = "Unknown Genre";
		require_once "../root/header.php";
		echo "<h1>Unknown Genre</h1>";
		echo "<p>This Genre does not exist!</p>";
		require_once "../root/footer.html"; 
		die();
	}
	else {
		$websiteTitle = $genre->getName();
	}	
	$sortartworksdir = null;
	$sortartworks = null;
	if (isset($_GET["sortartworksdir"])) {
		$sortartworksdir = SearchFunctions::validateParamInput($_GET["sortartworksdir"], "asc", array("asc", "desc"));
	}
	if (isset($_GET["sortartworks"])) {
		$sortartworks = SearchFunctions::validateParamInput($_GET["sortartworks"], "artworkid", array("title", "msrp", "year"));
	}
	$artworks = SearchFunctions::validateAdvancedSearchInput("artworks", null, null, null, null, $genre->getName(), null, $sortartworksdir, $sortartworks);

	require_once "../root/header.php";
?>

<h1><?php echo $websiteTitle ?></h1>

<div class="row">	
	<div class="col mt-3 mb-3">
		<div class="float-left d-inline mr-3 mb-3">
			<img src="<?php echo $genre->getPicture();  ?>" alt="Sample art of <?php echo $websiteTitle ?>" />
		</div>
		
		<p class="text-justify ml-3">
			<?php echo $genre->getDescription(); ?>
		</p>

		<p class="text-justify ml-3">
			More Info: <?php echo $genre->getLink(); ?>
		</p>
	</div>
</div>


<div class="row">
	<div class="col">
		<div class="d-block">
			<h2>Art of this genre</h2>
			<div class="mt-3 mb-3">
				<form action="../single/genre.php" method="get">
					<input type="hidden" name="id" value="<?php echo $id; ?>" />
					<?php echo SearchFunctions::getSortBox("artworks", array("title", "msrp", "year"), $sortartworksdir); ?>
				</form>
			</div>
			<div class="row">
				<?php echo SearchFunctions::showMatchingArtworks($artworks, "lg"); ?>
			</div>
		</div>
	</div>
</div>

<?php require_once "../root/footer.html"; ?>