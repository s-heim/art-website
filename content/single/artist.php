<?php 
	if (!isset($_SESSION)) {
		session_start();
	}

	require_once "../../processing/helper/prepare_functions.php";
	require_once "../../processing/helper/show_functions.php";
	require_once "../../processing/data/repositories/artists_repository.php";
	require_once "../../processing/data/repositories/artworks_repository.php";
	
	$id = 0;
	if (isset($_GET["id"])) {
		$id = PrepareFunctions::validateIdInput($_GET["id"]);
	}

	$artistrepo = new ArtistsRepository();
	$artist = $artistrepo->getArtistByID($id);

	if ($artist->getID() == null) {
		$websiteTitle = "Unknown Artist";
		require_once "../root/header.php";
		echo "<h1>Unknown Artist</h1>";
		echo "<p>This Artist does not exist!</p>";
		require_once "../root/footer.html"; 
		die();	
	}
	else {
		$websiteTitle = $artist->getName();
	}

	$artworkrepo = new ArtworksRepository();
	$artworks = $artworkrepo->getArtWorksForArtist($artist->getID());
	$artist->addArtworks($artworks);

	$favIsset = false;
	if (isset($_SESSION["UserID"])) {
		foreach($_SESSION["FavArtists"] as $fav) {
			if ($fav == $id) {
				$favIsset = true;
			}
		}
	}

	require_once "../root/header.php";
?>

<h1><?php echo $artist->getName() ?></h1>

<div class="row">	
	<div class="col mt-3 mb-3">
		<div class="float-left d-inline mr-3 mb-3">
			<img src='<?php echo $artist->getPicture(); ?>' alt='Portrait of <?php echo $artist->getName(true); ?>' />
		</div>
		<p class="text-justify ml-3">
			<?php echo $artist->getDetails(); ?>
		</p>
		<div class="mt-3 mb-3">
			<?php
				if (isset($_SESSION["UserID"])) {
					echo ShowFunctions::getFavoriteButton("FavArtists", $id, $favIsset);
				}
			?>
		</div>
		<table class="table table-hover d-inline">
			<thead>
				<tr>
					<th scope="col" colspan="2"><h4>Artist Details</h4></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th scope="row">Date:</td>
					<td><?php echo $artist->getDates(); ?></td>
				</tr>
				<tr>
					<th scope="row">Nationality:</td>
					<td><?php echo $artist->getNationality(); ?></td>
				</tr>
				<tr>
					<th scope="row">More Info:</td>
					<td><?php echo $artist->getLink(); ?></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col">
		<div class="d-block">
			<h2>Art by <?php echo $artist->getName(); ?></h2>

			<div class="row">
				<?php 
					$artworks = $artist->getArtWorks();
					if (count($artworks) != 0) {
						foreach($artworks as $artwork) {
							$artwork->setArtist($artist);
							echo "<div class='col-3'>";
							echo $artwork->getShortDetails("artist");
							echo "</div>";
						}
					}
					else {
						echo "<div class='col-3 font-italic'>(no artwork found)</div>";
					}
				?>
			</div>
		</div>
	</div>
</div>

<?php require_once "../root/footer.html"; ?>