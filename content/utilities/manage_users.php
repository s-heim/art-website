<?php
	if (!isset($_SESSION)) {
		session_start();
	}

	$websiteTitle = "Manage Users";
	require_once "../root/header.php"; 

	$_SESSION["UserDetails"] = null;

	require_once "../../processing/helper/user_functions.php"; 	
	
	if (!isset($_SESSION["UserID"])) {
		echo "<h1>Manage Users</h1>";
		echo "<p class='text-danger'>You must be logged in to use this feature! <a href='../utilites/login.php'>Login here!</a></p>";
		require_once "../root/footer.html"; 
		die (); 
	}
	else if (!($_SESSION["IsAdmin"])) {
		echo "<h1>Manage Users</h1>";
		echo "<p class='text-danger'>You are not allowed to use this feature!</p>";
		require_once "../root/footer.html"; 
		die ();
	}
	if (isset($_POST["deleteUser"])) {
		if ($_SESSION["UserID"] != $_POST["deleteUser"]) {
			UserFunctions::DeleteUser($_POST["deleteUser"]);
		}
	}
	if (isset($_POST["makeAdmin"])) {
		if ($_SESSION["UserID"] != $_POST["makeAdmin"]) {
			UserFunctions::MakeUserToAdmin($_POST["makeAdmin"]);
		}
	}
	if (isset($_POST["updateUser"])) {
		$userDetails = PrepareFunctions::getSingleUserDetails($_POST["updateUser"]);
		require_once "..//utilities/manage_single_user.php";
		die();
	}

	require_once "process_manage.php";
	require_once "../../processing/data/repositories/customers_repository.php";
		
	$customerrepo = new CustomersRepository();
	$users = $customerrepo->getAllCustomers();	
?>

<h1>Manage Users</h1>

<?php 
	if (isset($generalMessage)) {
		echo $generalMessage; 
	}
?>
	
<table class="table">
	<thead>
		<tr>
			<th scope="col"><h5>ID</h5></th>
			<th scope="col"><h5>Last Name</h5></th>
			<th scope="col"><h5>First Name</h5></th>
			<th scope="col"><h5>Country</h5></th>
			<th scope="col"><h5>Admin</h5></th>
			<th scope="col"><h5>Options</h5></th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach ($users as $user) {
				echo "<tr>";
				echo "<td>" . $user->getID() . "</td>";
				echo "<td>" . $user->getLastName() . "</td>";
				echo "<td>" . $user->getFirstName() . "</td>";
				echo "<td>" . $user->getCountry() . "</td>";
				if ($user->isAdmin()) {
					echo "<td>true</td>";
				}
				else {
					echo "<td>false</td>";
				}
				echo "<td>";
				echo "<form action='../utilities/manage_users.php' method='post'>";
				echo "<button type='submit' class='btn btn-sm btn-primary mr-3' name='updateUser' value='" . $user->getID() . "'>Update</button>";
				if ($user->getID() != $_SESSION["UserID"]) {
					echo "<button type='submit' class='btn btn-sm btn-primary mr-3' name='deleteUser' value='" . $user->getID() . "'>Delete</button>";
					if (!($user->isAdmin())) {
						echo "<button type='submit' class='btn btn-sm btn-primary mr-3' name='makeAdmin' value='" . $user->getID() . "'>Make admin</button>";
					}
				} 
				echo "</form>";
				echo "</td>";
				echo "</tr>";
			}
		?>
	</tbody>
</table>

<?php require_once "../root/footer.html"; ?>