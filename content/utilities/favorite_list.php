<?php 
	if (!isset($_SESSION)) {
		session_start();
	} 

	$websiteTitle = "Favorite List";
	require_once "../root/header.php"; 	
	
	if (!isset($_SESSION["UserID"])) { 
		echo "<h1>" . $websiteTitle . "</h1>";
		echo "<p class='text-danger'>You must be logged in to use this feature! <a href='../utilities/login.php' class='card-link'>Login here!</a></p>";
		require_once "../root/footer.html"; 
		die (); 
	} 
	
	require_once "../../processing/helper/show_functions.php"; 
	require_once "../../processing/helper/search_functions.php"; 	
	
	$sortartworksdir = null;
	$sortartistsdir = null;
	$sortartists = null;
	$sortartworks = null;
	$delfavartist = null;
	$delfavartwork = null;
	$artists = null;
	$artworks = null;
	
	if (isset($_GET["sortartworksdir"])) {
		$sortartworksdir = SearchFunctions::validateParamInput($_GET["sortartworksdir"], "asc", array("asc", "desc"));
	}
	if (isset($_GET["sortartistsdir"])) {
		$sortartistsdir = SearchFunctions::validateParamInput($_GET["sortartistsdir"], "asc", array("asc", "desc"));
	}
	if (isset($_GET["sortartists"])) {
		$sortartists = SearchFunctions::validateParamInput($_GET["sortartists"], "artistsid", array("last name"));
	}
	if (isset($_GET["sortartworks"])) {
		$sortartworks = SearchFunctions::validateParamInput($_GET["sortartworks"], "artworkid", array("title", "msrp", "year"));
	}
	if (isset($_GET["delfavartist"])) {
		$delfavartist = PrepareFunctions::validateIdInput($_GET["delfavartist"]);
		for ($i = 0; $i < count($_SESSION["FavArtists"]); $i++) {
			if ($_SESSION["FavArtists"][$i] == $delfavartist) {
				unset($_SESSION["FavArtists"][$i]);
				$_SESSION["FavArtists"] = array_values($_SESSION["FavArtists"]);
			}
		}
	}
	if (isset($_GET["delfavartwork"])) {
		$delfavartwork = PrepareFunctions::validateIdInput($_GET["delfavartwork"]);
		for ($i = 0; $i < count($_SESSION["FavArtworks"]); $i++) {
			if ($_SESSION["FavArtworks"][$i] == $delfavartwork) {
				unset($_SESSION["FavArtworks"][$i]);
				$_SESSION["FavArtworks"] = array_values($_SESSION["FavArtworks"]);
			}
		}
	}
	if (isset($_POST["FavArtists"])) {
		$_SESSION["FavArtists"][] = $_POST["FavArtists"];
	}
	if (isset($_POST["FavArtworks"])) {
		$_SESSION["FavArtworks"][] = $_POST["FavArtworks"];
	}
	$artists = SearchFunctions::getFavorites("artists", ($_SESSION["FavArtists"]), $sortartistsdir, $sortartists);
	$artworks = SearchFunctions::getFavorites("artworks", ($_SESSION["FavArtworks"]), $sortartworksdir, $sortartworks);
?>

<h1>Favorite List</h1>

<p>View your favorite artists and artworks:</p>

<div class="row">
	<?php if ($artists != null) { ?>
		<div class="col">
			<h2>Favorite Artists:</h2>
			<div class="mt-3 mb-3">
				<form action="../utilities/favorite_list.php" method="get">
					<input type="hidden" name="sortartworksdir" value="<?php echo $sortartworksdir; ?>" />
					<input type="hidden" name="sortartworks" value="<?php echo $sortartworks; ?>" />
					<?php echo SearchFunctions::getSortBox("artists", array("last name"), $sortartistsdir); ?>
				</form>
			</div>
			<div class="row">
				<?php 
					$size = "lg";
					if ($artworks != null) {
						$size = "sm";
					}
					echo SearchFunctions::showMatchingArtists($artists, $size, "delfavartist", $sortartworksdir, $sortartistsdir, $sortartists, $sortartworks); 
				?>
			</div>
		</div>
	<?php } ?>
	<?php if ($artworks != null) { ?>
		<div class="col">
			<h2>Favorite Artworks:</h2>
			<div class="mt-3 mb-3">
				<form action="../utilities/favorite_list.php" method="get">
					<input type="hidden" name="sortartistsdir" value="<?php echo $sortartistsdir; ?>" />
					<input type="hidden" name="sortartists" value="<?php echo $sortartists; ?>" />
					<?php echo SearchFunctions::getSortBox("artworks", array("title", "msrp", "year"), $sortartworksdir); ?>
				</form>
			</div>
			<div class="row">
				<?php 
					$size = "lg";
					if ($artists != null) {
						$size = "sm";
					}
					echo SearchFunctions::showMatchingArtworks($artworks, $size, "delfavartwork", $sortartworksdir, $sortartistsdir, $sortartists, $sortartworks); 
				?>
			</div>
		</div>
	<?php } ?>
</div>

<?php require_once "../root/footer.html"; ?>