<?php 
	if (!isset($_SESSION)) {
		session_start();
	}

	if (isset($_SESSION["UserID"])) {
		require_once "../root/header.php"; 
		echo "<h1>Register</h1>";
		echo "<p class='text-danger'>You are already registered!</p>";
		require_once "../root/footer.html"; 
		die ();
	}
	
	require_once "../../processing/helper/user_functions.php"; 

	$username = null;
	$usernameIsValid = null;
	$usernameMessage = null;
	$email = null;
	$emailIsValid = null; 
	$emailMessage = null;
	$pass = null;
	$passIsValid = null;
	$passMessage = "<small class='form-text text-muted'>Must be at least 8 characters long, contain big and small letters and numbers.</small>";
	$phone = null;
	$phoneIsValid = null;
	$firstname = null;
	$firstnameIsValid = null;
	$lastname = null;
	$lastnameIsValid = null;
	$address = null;
	$addressIsValid = null;
	$city = null;
	$cityIsValid = null;
	$region = null;
	$regionIsValid = null;
	$country = null;
	$countryIsValid = null;
	$postal = null;
	$postalIsValid = null;
	$generalMessage = null;
	
	if (isset($_POST["tryRegister"])) {
		if (isset($_POST["username"])) {
			if (UserFunctions::validateRegisterInput($_POST["username"], "username")) {
				$username = $_POST["username"];
				$usernameIsValid = "is-valid";
				$usernameMessage = "<div class='valid-feedback'>Username available!</div>";
			}
			else {
				$usernameIsValid = "is-invalid";
				$usernameMessage = "<div class='invalid-feedback'>Username already used!</div>";
			}
		}
		if (isset($_POST["pass"])) {
			if (UserFunctions::validateRegisterInput($_POST["pass"], "password")) {
				$pass = $_POST["pass"];
				$passIsValid = "is-valid";
				$passMessage = "<div class='valid-feedback'>Password secure!</div>";
			}
			else {
				$passIsValid = "is-invalid";
				$passMessage = "<div class='invalid-feedback'>Must be at least 8 characters long, contain big and small letters and numbers.</div>";
			}
		}
		if (isset($_POST["email"])) {
			if (UserFunctions::validateRegisterInput($_POST["email"], "email")) {
				$email = $_POST["email"];
				$emailIsValid = "is-valid";
				$emailMessage = "<div class='valid-feedback'>Email available!</div>";
			}
			else {
				$emailIsValid = "is-invalid";
				$emailMessage = "<div class='invalid-feedback'>Email not an email or already used!</div>";
			}
		}
		if (isset($_POST["phone"])) {
			if (UserFunctions::validateRegisterInput($_POST["phone"], "")) {
				$phone = $_POST["phone"];
				$phoneIsValid = "is-valid";
			}
		}
		if (isset($_POST["lastname"])) {
			if (UserFunctions::validateRegisterInput($_POST["lastname"], "")) {
				$lastname = $_POST["lastname"];
				$lastnameIsValid = "is-valid";
			}
		}
		if (isset($_POST["firstname"])) {
			if (UserFunctions::validateRegisterInput($_POST["firstname"], "")) {
				$firstname = $_POST["firstname"];
				$firstnameIsValid = "is-valid";
			}
		}
		if (isset($_POST["address"])) {
			if (UserFunctions::validateRegisterInput($_POST["address"], "")) {
				$address = $_POST["address"];
				$addressIsValid = "is-valid";
			}
		}
		if (isset($_POST["postal"])) {
			if (UserFunctions::validateRegisterInput($_POST["postal"], "")) {
				$postal = $_POST["postal"];
				$postalIsValid = "is-valid";
			}
		}
		if (isset($_POST["city"])) {
			if (UserFunctions::validateRegisterInput($_POST["city"], "")) {
				$city = $_POST["city"];
				$cityIsValid = "is-valid";
			}
		}
		if (isset($_POST["region"])) {
			if (UserFunctions::validateRegisterInput($_POST["region"], "")) {
				$region = $_POST["region"];
				$regionIsValid = "is-valid";
			}
		}
		if (isset($_POST["country"])) {
			if (UserFunctions::validateRegisterInput($_POST["country"], "")) {
				$country = $_POST["country"];
				$countryIsValid = "is-valid";
			}
		}
		if (isset($username) && isset($pass) && isset($email) && isset($lastname) && isset($address) && isset($country)) {
			if (UserFunctions::AddUser($username, $pass, $firstname, $lastname, $address, $city, $region, $country, $postal, $phone, $email, 1)) {
				$registredNow = true;
				header('Location: ../utilities/login.php');
				die();
			}
			else {
				$generalMessage = "<p class='text-danger'>Registration failure!</p>";
			}
		}
		else {
			$generalMessage = "<p class='text-danger'>Registration not possible!</p>";
		}
	}

	$websiteTitle = "Register";
	require_once "../root/header.php"; 
?>

<h1>Register</h1>

<?php echo $generalMessage; ?>

<form action="../utilities/register.php" method="post" >
	<div class="row">
		<div class="col-4">
			<input type="hidden" name="tryRegister" value=true />
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="username">Username</label>
						<input type="text" class="form-control <?php echo $usernameIsValid; ?>" id="username" name="username" value="<?php echo $username; ?>" required />
						<?php echo $usernameMessage; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="email">E-Mail</label> 
						<input type="email" class="form-control <?php echo $emailIsValid; ?>" id="email" name="email" value="<?php echo $email; ?>"  required />
						<?php echo $emailMessage; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="pass">Password</label>
						<input type="password" class="form-control <?php echo $passIsValid; ?>" id="pass" name="pass" required />
						<?php echo $passMessage; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="phone">Phone</label>
						<input type="text" class="form-control <?php echo $phoneIsValid; ?>" id="phone" name="phone" value="<?php echo $phone; ?>" />
						
					</div>
				</div>
			</div>
		</div>
		<div class="col-8">
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="lastname">Last Name</label>
						<input type="text" class="form-control <?php echo $lastnameIsValid; ?>" id="lastname" name="lastname" value="<?php echo $lastname; ?>" required />
						<?php 
							if (isset($usernameMessage)) {
								echo "<br />";
							} 
						?>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="firstname">First Name</label>
						<input type="text" class="form-control <?php echo $firstnameIsValid; ?>" id="firstname"  name="firstname" value="<?php echo $firstname; ?>" />
						<?php 
							if (isset($usernameMessage)) {
								echo "<br />";
							} 
						?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="address">Address</label>
						<input type="text" class="form-control <?php echo $addressIsValid; ?>" id="address" name="address" value="<?php echo $address; ?>" required />
						<?php 
							if (isset($emailMessage)) { 
								echo "<br />"; 
							} 
						?>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="postal">Postal</label>
						<input type="text" class="form-control <?php echo $postalIsValid; ?>" id="postal" name="postal" value="<?php echo $postal; ?>"  />
						<?php 
							if (isset($emailMessage)) { 
								echo "<br />"; 
							} 
						?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="city">City</label>
						<input type="text" class="form-control <?php echo $cityIsValid; ?>" id="city" name="city" value="<?php echo $city; ?>" required />
						<?php 
							if (isset($passMessage)) { 
								echo "<br />"; 
							} 
						?>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="region">Region</label>
						<input type="text" class="form-control <?php echo $regionIsValid; ?>" id="region" name="region" value="<?php echo $region; ?>" />
						<?php 
							if (isset($passMessage)) { 
								echo "<br />"; 
							} 
						?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="country">Country</label>
						<input type="tel" class="form-control <?php echo $countryIsValid; ?>" id="country" name="country" value="<?php echo $country; ?>" required />
					</div>
				</div>
			</div>
		</div>
	</div>
	<button type="submit" class="btn btn-success">Register</button>
</form>

<?php require_once "../root/footer.html"; ?>