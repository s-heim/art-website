<?php 
	if (!isset($_SESSION)) {
		session_start();
	}
	if (!isset($_SESSION["UserID"])) {
		echo "<h1>Manage Users</h1>";
		echo "<p class='text-danger'>You must be logged in to use this feature! <a href='../utilities/login.php'>Login here!</a></p>";
		require_once "../root/footer.html"; 
		die (); 
	}
	else if (!($_SESSION["IsAdmin"])) {
		echo "<h1>Manage Users</h1>";
		echo "<p class='text-danger'>You are not allowed to use this feature!</p>";
		require_once "../root/footer.html"; 
		die ();
	}
	
	$id = null;
	$username = null;
	$email = null;
	$pass = null;
	$phone = null;
	$firstname = null;
	$lastname = null;
	$address = null;
	$city = null;
	$region = null;
	$country = null;
	$postal = null;
	$privacy = null;
	$phoneIsValid = null;
	$firstnameIsValid = null;
	$lastnameIsValid = null;
	$addressIsValid = null;
	$cityIsValid = null;
	$regionIsValid = null;
	$countryIsValid = null;
	$postalIsValid = null;
	$generalMessage = null;
	
	if (isset($userDetails)) {
		$id = $userDetails->getID();
		$username = $userDetails->getUserName();
		$email = $userDetails->getEmail();
		$phone = $userDetails->getPhone();
		$firstname = $userDetails->getFirstName();
		$lastname = $userDetails->getLastName();
		$address = $userDetails->getAddress();
		$city = $userDetails->getCity();
		$region = $userDetails->getRegion();
		$country = $userDetails->getCountry();
		$postal = $userDetails->getPostal();
		$privacy = $userDetails->getPrivacy();
		
	}
	
	if (isset($_POST["try"])) {
		if (isset($_POST["id"])) {
			$id = $_POST["id"];
		}
		if (isset($_POST["username"])) {
			$username = $_POST["username"];
		}
		if (isset($_POST["pass"])) {
			$pass = $_POST["pass"];
		}
		if (isset($_POST["email"])) {
			$email = $_POST["email"];
		}
		if (isset($_POST["phone"])) {
			if (UserFunctions::validateRegisterInput($_POST["phone"], "")) {
				$phone = $_POST["phone"];
				$phoneIsValid = "is-valid";
			}
		}
		if (isset($_POST["lastname"])) {
			if (UserFunctions::validateRegisterInput($_POST["lastname"], "")) {
				$lastname = $_POST["lastname"];
				$lastnameIsValid = "is-valid";
			}
		}
		if (isset($_POST["firstname"])) {
			if (UserFunctions::validateRegisterInput($_POST["firstname"], "")) {
				$firstname = $_POST["firstname"];
				$firstnameIsValid = "is-valid";
			}
		}
		if (isset($_POST["address"])) {
			if (UserFunctions::validateRegisterInput($_POST["address"], "")) {
				$address = $_POST["address"];
				$addressIsValid = "is-valid";
			}
		}
		if (isset($_POST["postal"])) {
			if (UserFunctions::validateRegisterInput($_POST["postal"], "")) {
				$postal = $_POST["postal"];
				$postalIsValid = "is-valid";
			}
		}
		if (isset($_POST["city"])) {
			if (UserFunctions::validateRegisterInput($_POST["city"], "")) {
				$city = $_POST["city"];
				$cityIsValid = "is-valid";
			}
		}
		if (isset($_POST["region"])) {
			if (UserFunctions::validateRegisterInput($_POST["region"], "")) {
				$region = $_POST["region"];
				$regionIsValid = "is-valid";
			}
		}
		if (isset($_POST["country"])) {
			if (UserFunctions::validateRegisterInput($_POST["country"], "")) {
				$country = $_POST["country"];
				$countryIsValid = "is-valid";
			}
		}
		if (isset($username) && isset($pass) && isset($email) && isset($lastname) && isset($address) && isset($country)) {
			if (UserFunctions::EditUser($id, $firstname, $lastname, $address, $city, $region, $country, $postal, $phone, $email, $privacy)) {
				$generalMessage = "<p class='text-success'>Update successfull!</p>";
				require_once "../utilities/manage_users.php"; 
			}
			else {
				$generalMessage = "<p class='text-danger'>Update failure!</p>";
				require_once "../utilities/manage_single_user.php"; 
				die();
			}
		}
		else {
			$generalMessage = "<p class='text-danger'>Update not possible!</p>";
			require_once "../utilities/manage_single_user.php"; 
			die();
		}
	}
	
?>