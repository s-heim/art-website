<?php
	if (!isset($_SESSION)) {
		session_start();
	}

	if (isset($_SESSION["UserID"])) {
		require_once "../root/header.php"; 
		echo "<h1>Login</h1>";
		echo "<p class='text-danger'>You are already logged in!</p>";
		require_once "../root/footer.php"; 
		die ();
	}
	require_once "../../processing/helper/user_functions.php";
	$user = null;
	$pass = null;
	
	if (isset($_POST["tryLogin"])) {	
		if (isset($_POST["username"]) && isset($_POST["pass"])) {
			$user = $_POST["username"];
			$pass = $_POST["pass"];
			if (UserFunctions::verifyUser($user, $pass)) {
				$customerrepo = new customersRepository();
				$customer = $customerrepo->getCustomersByName($user);
				$_SESSION["UserID"] = $customer->getID();
				$_SESSION["IsAdmin"] = $customer->isAdmin();
				$_SESSION["FavArtworks"] = array();
				$_SESSION["FavArtists"] = array();
				$loggedInNow = true;
			}
			else {
				$loginmessage = "Login failure!";
			}
		}
	}

	$websiteTitle = "Login";
	require_once "../root/header.php"; 
?>

<h1>Login</h1>

<?php 
	if (isset($loggedInNow) && $loggedInNow) {
		echo "<p class='text-success'>You are successfull logged in!</p>";
		echo "<p class='font-italic'><a href='../root/' class='card-link'>Back to Start Page</a></p>";
		require_once "../root/footer.html"; 
		die();
	}
	if (isset($registredNow) && $registredNow) {
		echo "<p class='text-success'>You are successfull registred, you can log in now!</p>";
	}
	if (isset($loginmessage)) {
		echo "<p class='text-danger'>" . $loginmessage . "</p>";
	}
?>

<form action="../utilities/login.php" method="post">
	<input type="hidden" name="tryLogin" value=true />
	<div class="row">
		<div class="col-4">
			<div class="form-group">
				<label for="username">Username</label>
				<input type="text" class="form-control" id="username" name="username" value="<?php echo $user; ?>" required />
			</div>
			<div class="form-group">
				<label for="pass">Password</label>
				<input type="password" class="form-control" id="pass" name="pass" required />
			</div>
			<button type="submit" class="btn btn-success">Login</button>
		</div>
	</div>
</form>

<p>Not registred yet? Then <a href="Register.php" class="card-link">Register</a> now!</p>

<?php require_once "../root/footer.html"; ?>