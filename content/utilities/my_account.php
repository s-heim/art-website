<?php 
	if (!isset($_SESSION)) {
		session_start();
	}

	$websiteTitle = 'My Account';
	require_once "../root/header.php";

	if (!isset($_SESSION["UserID"])) { 
		echo "<h1>" . $websiteTitle . "</h1>";
		echo "<p class='text-danger'>You must be logged in to use this feature! <a href='../utilities/login.php' class='card-link'>Login here!</a></p>";
		require_once "../root/footer.html"; 
		die (); 
	} 

	require_once "../../processing/helper/user_functions.php"; 

	$userDetails = PrepareFunctions::getSingleUserDetails($_SESSION["UserID"]);
	$id = $userDetails->getID();
	$username = $userDetails->getUserName();
	$email = $userDetails->getEmail();
	$phone = $userDetails->getPhone();
	$oldpass = null;
	$newpass = null;
	$firstname = $userDetails->getFirstName();
	$lastname = $userDetails->getLastName();
	$address = $userDetails->getAddress();
	$city = $userDetails->getCity();
	$region = $userDetails->getRegion();
	$country = $userDetails->getCountry();
	$postal = $userDetails->getPostal();
	$privacy = $userDetails->getPrivacy();
	$usernameIsValid = null;
	$passIsValid = null;
	$oldPassIsValid = null;
	$newPassIsValid = null;
	$emailIsValid = null;
	$phoneIsValid = null;
	$firstnameIsValid = null;
	$lastnameIsValid = null;
	$addressIsValid = null;
	$cityIsValid = null;
	$regionIsValid = null;
	$countryIsValid = null;
	$postalIsValid = null;
	$usernameMessage = null;
	$passMessage = "<small class='form-text text-muted'>Type in your current password to confirm the update.</small>";
	$oldPassMessage = "<small class='form-text text-muted'>Type in your current password to confirm the update.</small>";
	$newPassMessage = "<small class='form-text text-muted'>Your password must be at least 8 characters long, contain letters and numbers.</small>";
	$emailMessage = null;
	$generalMessage = null;
	$generalPassMessage = null;
	
	if (isset($_POST["tryUpdate"])) {
		if (isset($_POST["username"])) {
			if (UserFunctions::validateRegisterInput($_POST["username"], "username") || $_POST["username"] == $username) {
				$username = $_POST["username"];
				$usernameIsValid = "is-valid";
				$usernameMessage = "<div class='valid-feedback'>Username available!</div>";
			}
			else {
				$usernameIsValid = "is-invalid";
				$usernameMessage = "<div class='invalid-feedback'>Username already used!</div>";
			}
		}
		if (isset($_POST["pass"])) {
			if (UserFunctions::verifyUser($username, $_POST["pass"]))  {
				$passIsValid = "is-valid";
				$passMessage = "<div class='valid-feedback'>Password correct!</div>";
			}
			else {
				$passIsValid = "is-invalid";
				$passMessage = "<div class='invalid-feedback'>Type in your current password to confirm the update.</div>";
			}
		}
		if (isset($_POST["email"])) {
			if (UserFunctions::validateRegisterInput($_POST["email"], "email")  || $_POST["email"] == $email) {
				$email = $_POST["email"];
				$emailIsValid = "is-valid";
				$emailMessage = "<div class='valid-feedback'>Email available!</div>";
			}
			else {
				$emailIsValid = "is-invalid";
				$emailMessage = "<div class='invalid-feedback'>Email already used!</div>";
			}
		}
		if (isset($_POST["phone"])) {
			if (UserFunctions::validateRegisterInput($_POST["phone"], "")) {
				$phone = $_POST["phone"];
				$phoneIsValid = "is-valid";
			}
		}
		if (isset($_POST["lastname"])) {
			if (UserFunctions::validateRegisterInput($_POST["lastname"], "")) {
				$lastname = $_POST["lastname"];
				$lastnameIsValid = "is-valid";
			}
		}
		if (isset($_POST["firstname"])) {
			if (UserFunctions::validateRegisterInput($_POST["firstname"], "")) {
				$firstname = $_POST["firstname"];
				$firstnameIsValid = "is-valid";
			}
		}
		if (isset($_POST["address"])) {
			if (UserFunctions::validateRegisterInput($_POST["address"], "")) {
				$address = $_POST["address"];
				$addressIsValid = "is-valid";
			}
		}
		if (isset($_POST["postal"])) {
			if (UserFunctions::validateRegisterInput($_POST["postal"], "")) {
				$postal = $_POST["postal"];
				$postalIsValid = "is-valid";
			}
		}
		if (isset($_POST["city"])) {
			if (UserFunctions::validateRegisterInput($_POST["city"], "")) {
				$city = $_POST["city"];
				$cityIsValid = "is-valid";
			}
		}
		if (isset($_POST["region"])) {
			if (UserFunctions::validateRegisterInput($_POST["region"], "")) {
				$region = $_POST["region"];
				$regionIsValid = "is-valid";
			}
		}
		if (isset($_POST["country"])) {
			if (UserFunctions::validateRegisterInput($_POST["country"], "")) {
				$country = $_POST["country"];
				$countryIsValid = "is-valid";
			}
		}
		if (isset($username) && isset($email) && isset($lastname) && isset($address) && isset($country) && ($passIsValid == "is-valid")) {
			if (UserFunctions::EditUser($id, $firstname, $lastname, $address, $city, $region, $country, $postal, $phone, $email, $privacy)) {
				$generalMessage = "<p class='text-success'>Update successfull!</p>";
			}
			else {
				$generalMessage = "<p class='text-danger'>Update failure!</p>";
				require_once "../utilities/my_account.php";
			}
		}
		else {	
			$generalMessage = "<p class='text-danger'>Update not possible!</p>";
		}
	}
	
	if (isset($_POST["tryPass"])) {
		if (isset($_POST["oldpass"])) {
			if (UserFunctions::verifyUser($username, $_POST["oldpass"]))  {
				$oldpass = $_POST["oldpass"];
				$oldPassIsValid = "is-valid";
				$oldPassMessage = "<div class='valid-feedback'>Password correct!</div>";
			}
			else {
				$oldPassIsValid = "is-invalid";
				$oldPassMessage = "<div class='invalid-feedback'>Type in your current password to confirm the update.</div>";
			}
		}
		if (isset($_POST["newpass"])) {
			if (UserFunctions::validateRegisterInput($_POST["newpass"], "password")) {
				$newpass = $_POST["newpass"];
				$newPassIsValid = "is-valid";
				$newPassMessage = "<div class='valid-feedback'>Password secure!</div>";
			}
			else {
				$newPassIsValid = "is-invalid";
				$newPassMessage = "<div class='invalid-feedback'>Your password must be at least 8 characters long, contain letters and numbers.</div>";
			}
		}
		if (isset($oldpass) && isset($newpass)) {
			if(UserFunctions::EditUserPass($id, $username, $oldpass, $newpass)) {
				$generalPassMessage = "<p class='text-success'>Update successfull!</p>";
			}
			else {
				$generalPassMessage = "<p class='text-danger'>Update failure!</p>";
				require_once "../utilities/my_account.php";
			}
		}
		else {	
			$generalPassMessage = "<p class='text-danger'>Update not possible!</p>";
		}
	}
?>

<h1>My Account</h1>

<p>Here you get a overview about your Account Information.</p>

<h2>Update Password</h2>

<?php echo $generalPassMessage; ?>

<form action="../utilities/my_account.php" class="form-row mt-3 mb-3" method="post" >
	<input type="hidden" name="tryPass" value=true />
	<div class="col">		
		<input type="password" class="form-control" placeholder="Old Password" name="oldpass" required />
	</div>
	<div class="col">
		<input type="password" class="form-control" placeholder="New Password" name="newpass" required />
	</div>
	<div class="col">		
		<button type="submit" class="btn btn-success mb-3" name="update-password">Update</button>
	</div>
</form>

<h2>Update Personal Data</h2>

<?php echo $generalMessage; ?>

<form class="mt-3 mb-3" action="../utilities/my_account.php" method="post" >
	<input type="hidden" name="tryUpdate" value=true />
	<input type="hidden" name="pass" value="<?php echo $pass; ?>" />
	<div class="row">
		<div class="col-4">
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="username">Username</label>
						<input type="text" class="form-control <?php echo $usernameIsValid; ?>" id="username" name="username" value="<?php echo $username; ?>" required />
						<?php echo $usernameMessage; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="email">E-Mail</label> 
						<input type="email" class="form-control <?php echo $emailIsValid; ?>" id="email"  name="email" value="<?php echo $email; ?>"  required />
						<?php echo $emailMessage; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="pass">Password</label>
						<input type="password" class="form-control <?php echo $passIsValid; ?>" id="pass" name="pass" required />
						<?php echo $passMessage; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="phone">Phone</label>
						<input type="text" class="form-control <?php echo $phoneIsValid; ?>" id="phone" name="phone" value="<?php echo $phone; ?>" />
						
					</div>
				</div>
			</div>
		</div>
		<div class="col-8">
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="lastname">Last Name</label>
						<input type="text" class="form-control <?php echo $lastnameIsValid; ?>" id="lastname" name="lastname" value="<?php echo $lastname; ?>" required />
						<?php 
							if (isset($usernameMessage)) {
								echo "<br />";
							} 
						?>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="firstname">First Name</label>
						<input type="text" class="form-control <?php echo $firstnameIsValid; ?>" id="firstname" name="firstname" value="<?php echo $firstname; ?>" />
						<?php 
							if (isset($usernameMessage)) {
								echo "<br />";
							} 
						?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="address">Address</label>
						<input type="text" class="form-control <?php echo $addressIsValid; ?>" id="address" name="address" value="<?php echo $address; ?>" required />
						<?php 
							if (isset($emailMessage)) { 
								echo "<br />"; 
							} 
						?>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="postal">Postal</label>
						<input type="text" class="form-control <?php echo $postalIsValid; ?>" id="postal" name="postal" value="<?php echo $postal; ?>"  />
						<?php 
							if (isset($emailMessage)) { 
								echo "<br />"; 
							} 
						?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="city">City</label>
						<input type="text" class="form-control <?php echo $cityIsValid; ?>" id="city" name="city" value="<?php echo $city; ?>" required />
						<?php 
							if (isset($passMessage)) { 
								echo "<br />"; 
							} 
						?>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="region">Region</label>
						<input type="text" class="form-control <?php echo $regionIsValid; ?>" id="region" name="region" value="<?php echo $region; ?>" />
						<?php 
							if (isset($passMessage)) { 
								echo "<br />"; 
							} 
						?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="country">Country</label>
						<input type="tel" class="form-control <?php echo $countryIsValid; ?>" id="country" name="country" value="<?php echo $country; ?>" required />
					</div>
				</div>
			</div>
		</div>
	</div>
	<button type="submit" class="btn btn-success mb-3" name="update-account">Update</button>
</form>

<?php require_once "../root/footer.html"; ?>