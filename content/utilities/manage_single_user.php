<?php
	if (!isset($_SESSION)) {
		session_start();
	}
	$websiteTitle = "Manage Users";
	require_once "../root/header.php"; 	
	require_once "process_manage.php";
	
?>

<h1>Manage Users</h1>

<?php echo $generalMessage; ?>

<form action="../utilities/manage_users.php" method="post" >
	<div class="row">
		<div class="col-4">
			<input type="hidden" name="try" value=true />
			<input type="hidden" name="id" value="<?php echo $id; ?>" />
			<input type="hidden" name="pass" value="<?php echo $pass; ?>" />
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="username">Username</label>
						<input type="text" class="form-control" id="username" name="username" value="<?php echo $username; ?>" readonly />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="email">E-Mail</label> 
						<input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>"  readonly />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="pass">Password</label>
						<input type="password" class="form-control <?php echo $passIsValid; ?>" id="pass" name="pass" value="<?php echo substr($pass, 0, 8); ?>" readonly />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="phone">Phone</label>
						<input type="text" class="form-control <?php echo $phoneIsValid; ?>" id="phone" name="phone" value="<?php echo $phone; ?>" />
						
					</div>
				</div>
			</div>
		</div>
		<div class="col-8">
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="lastname">Last Name</label>
						<input type="text" class="form-control <?php echo $lastnameIsValid; ?>" id="lastname" name="lastname" value="<?php echo $lastname; ?>" required />
						<?php if (isset($usernameMessage)) {
							echo "<br />";
						} ?>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="firstname">First Name</label>
						<input type="text" class="form-control <?php echo $firstnameIsValid; ?>" id="firstname" name="firstname" value="<?php echo $firstname; ?>" />
						<?php if (isset($usernameMessage)) {
							echo "<br />";
						} ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="address">Address</label>
						<input type="text" class="form-control <?php echo $addressIsValid; ?>" id="address" name="address" value="<?php echo $address; ?>" required />
						<?php if (isset($emailMessage)) { echo "<br />"; } ?>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="postal">Postal</label>
						<input type="text" class="form-control <?php echo $postalIsValid; ?>" id="postal" name="postal" value="<?php echo $postal; ?>"  />
						<?php if (isset($emailMessage)) { echo "<br />"; } ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="city">City</label>
						<input type="text" class="form-control <?php echo $cityIsValid; ?>" id="city" name="city" value="<?php echo $city; ?>" required />
						<?php if (isset($passMessage)) { echo "<br />"; } ?>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="region">Region</label>
						<input type="text" class="form-control <?php echo $regionIsValid; ?>" id="region" name="region" value="<?php echo $region; ?>" />
						<?php if (isset($passMessage)) { echo "<br />"; } ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="country">Country</label>
						<input type="tel" class="form-control <?php echo $countryIsValid; ?>" id="country" name="country" value="<?php echo $country; ?>" required />
					</div>
				</div>
			</div>
		</div>
	</div>
	<button type="submit" class="btn btn-success mb-3" name="update-customer">Update</button>
</form>

<form action="../utilities/manage_users.php" method="post">
	<button type="submit" class="btn btn-primary mt-3">Back to Overview</button>
</form>

<?php require_once "../root/footer.html"; ?>