<?php
	if (!isset($_SESSION)) {
		session_start();
	}
	$_SESSION["UserID"] = null;
	$_SESSION["IsAdmin"] = null;
	session_destroy();
	$websiteTitle = "Logout";
	require_once "../root/header.php"; 
	
?>

<h1>Logout</h1>

<p class='text-success'>You are successfull logged out!</p>
<p class="font-italic"><a href="../utilities/login.php" class="card-link">Back to Login</a></p>

<?php require_once "../root/footer.html"; ?>