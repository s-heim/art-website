# ART Website

This repository is the result of an assignment work in the winter term of 2020/21 during my Bachelor's degree.

In the task, a database script and a set of images were given, and an informative art website was to be created.

## Implementation

The website is mainly implemented on the server side with PHP, a MySQL/MariaDB database is used. The frontend is designed with [Bootstrap](https://getbootstrap.com/).

## Get running

This application requires an APACHE webserver with PHP support and a database like MySQL/MariaDB and PHP support. The essential steps for the setup are:

1. Download or clone the repository into the served folder.
2. Unpack `img.zip` into the same folder
3. Create a database (and a user for the permissions to read if needed)
4. Run the [art_db.sql](processing/data/configuration/art_db.sql) script to setup the database
5. Update the [config.php](processing/data/configuration/config.php)
6. Run the server and access the website at <http://localhost:8080/art-website/content/root/index.php>

The default users including the administrator ("admin") are initialized with the password `abcd1234` in the database, it is also possible to add own users.

## Screenshots

![](screenshots/homepage.png)

![](screenshots/browse-artist.png)

![](screenshots/single-artist.png)

![](screenshots/artwork.png)

![](screenshots/my-account.png)
